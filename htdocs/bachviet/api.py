from tastypie.resources import ModelResource
from tastypie.authorization import DjangoAuthorization, Authorization
from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication, DigestAuthentication
from tastypie.models import ApiKey, create_api_key
from django.db import models
from bachviet.apps.tt_dictionary.models import Word, WordClass, TTSearchEngine, SearchTest

class testSearch(ModelResource):
    class Meta:
        #test = SearchTest()
        #queryset = test.search1('',text='hallo')
        queryset = Word.objects.all()
        resource_name = 'testsearch'
        authorization = DjangoAuthorization()
        authentication = Authentication()
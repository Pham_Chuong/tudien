from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from tastypie.api import Api
from bachviet.api import testSearch


from bachviet.apps.tt_dictionary import views as dictionary_views

v1_api = Api(api_name='v1')
v1_api.register(testSearch())


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bachviet.views.home', name='home'),
    # url(r'^bachviet/', include('bachviet.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    #url(r'^index/', dictionary_views.index, name='index'),
    url(r'^test/', dictionary_views.test, name='test'),
    url(r'^test1/', dictionary_views.test1, name='test1'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', dictionary_views.index, name='index'),
    url(r'^words$', dictionary_views.words, name='words'),
    url(r'^result/', dictionary_views.result, name='result'),
    url(r'^list/', dictionary_views.list, name='list'),
    url(r'^detail/', dictionary_views.detail, name='detail'),
    url(r'^detail1/', dictionary_views.detail1, name='detail1'),
    url(r'^detail3/', dictionary_views.detail3, name='detail3'),
    url(r'^listdetail/', dictionary_views.listdetail, name='listdetail'),
    url(r'^detail2/', dictionary_views.detail2, name='detail2'),
    url(r'^search$', dictionary_views.search, name='search'),
    url(r'^listfield', dictionary_views.listfield, name='listfield'),
    url(r'^api/', include(v1_api.urls)),
    
)

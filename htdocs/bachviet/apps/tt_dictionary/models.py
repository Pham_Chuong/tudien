# -*- coding: UTF-8 -*- 

from django.db import models, connection
from django.utils.encoding import smart_unicode
from bachviet.libs.utils import dictFetchAll, StringWithTitle
import re, MySQLdb

class TTDictionaryMeta():
    app_label = StringWithTitle("tt_dictionary", "Bach Viet Dictionary")
    
class Branche(models.Model):
    text = models.TextField(blank=True)
    
    def __unicode__(self):
        return self.text
    
    class Meta(TTDictionaryMeta):
        db_table = u'Branche'
        
class Example(models.Model):
    text = models.TextField(blank=True)
    status = models.IntegerField(default=0)
    wordTranslateId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'Example'
        
class ExampleTranslate(models.Model):
    text = models.TextField(blank=True)
    status = models.IntegerField(default=0)
    exampleId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'ExampleTranslate'
        
class ExampleTranslateType(models.Model):
    exampleTranslateId = models.IntegerField()
    typeId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'ExampleTranslateType'
        
class Grammar(models.Model):
    status = models.IntegerField(default=0)
    wordClassId = models.IntegerField()
    wordId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'Grammar'
        
class GrammarDetails(models.Model):
    name = models.CharField(max_length=45)
    text = models.TextField(blank=True)
    grammarId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'GrammarDetails'
        verbose_name = u'Grammar Detail'
        verbose_name_plural = u'Grammar Details'
        
class GrammarHeader(models.Model):
    name = models.CharField(max_length=45)
    text = models.TextField(blank=True)
    link = models.CharField(max_length=45)
    grammarId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'GrammarHeader'
        
class Media(models.Model):
    path = models.TextField(blank=True)
    type = models.CharField(max_length=255)
    wordId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'Media'
        
class SearchHistory(models.Model):
    text = models.CharField(max_length=50)
    typeId = models.CharField(max_length=255)
    tstamp = models.DateTimeField(auto_now_add=True)
    lang = models.IntegerField(max_length=1, default=1)
    status = models.IntegerField(max_length=1, default=1)
    class Meta(TTDictionaryMeta):
        db_table = u'SearchHistory'
        
class Type(models.Model):
    text = models.TextField(blank=True)
    brancheId = models.IntegerField()
    pid = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.text
    
    class Meta(TTDictionaryMeta):
        db_table = u'Type'
    
class Word(models.Model):
    text = models.TextField(blank=True)
    lang = models.IntegerField(max_length=11, default=1)
    status = models.IntegerField(max_length=11, default=0)
    
    def __unicode__(self):
        return self.text
        
    def getWordTranslates(self):
        word_translates = WordTranslate.objects.filter(wordId=self.id)
        for w in word_translates:
            print w.text
            
        return word_translates
        
    class Meta(TTDictionaryMeta):
        db_table = u'Word'
        
class WordClass(models.Model):
    title = models.CharField(max_length=45)
    description = models.CharField(max_length=45)
    
    def __unicode__(self):
        return self.title
        
    class Meta(TTDictionaryMeta):
        db_table = u'WordClass'
    
class WordTranslate(models.Model):
    text = models.TextField(blank=True)
    status = models.IntegerField(max_length=11, default=0)
    wordId = models.IntegerField()
    
    def __unicode__(self):
        return self.text
    
    class Meta(TTDictionaryMeta):
        db_table = u'WordTranslate'
        
class WordTranslateType(models.Model):
    wordTranslateId = models.IntegerField()
    typeId = models.IntegerField()
    class Meta(TTDictionaryMeta):
        db_table = u'WordTranslateType'
        
class WordHistory(models.Model):
    tstamp = models.DateTimeField(auto_now_add=True)
    wordId = models.IntegerField(max_length=11)
    status = models.IntegerField(max_length=11)
    updateType = models.CharField(max_length=45)
    updateDescription = models.CharField(max_length=255)
    class Meta(TTDictionaryMeta):
        db_table = u'WordHistory'

def addItemSearch(word, field, lang, status):
    result = []
    cursor = connection.cursor()
    queryString = TTSearchProvider.addItem(word, field, lang, status)
    cursor.execute(queryString)
    
          
class TTSearchProvider():
    def __str__(self):
        return u'Search Provider Class'
        
    @staticmethod
    def queryAlgorithm1(text='', branche='', wclass='', lang=1):
        fields = ''
        sql_add = ''
        if branche:
            fields = 'AND T.id IN (%s) ' % branche
            
        if wclass: 
            wclass = 'AND WC.id IN (%s) ' % wclass
            
        search_text_no_plus = re.sub('[+]', '', text)
        
        if lang == 1: #German
            if (len(search_text_no_plus) > 1):
                expL = "(^|[0-9]{1}[.]{1}[ ]{0,2}|[,]{1}[ ]{0,2}|[[.less-than-sign.]]{1}(.*)[[.greater-than-sign.]]{1}[ ]{0,2})"
                expR = "($|[,][ ]{0,2}|[ ]{0,2}[[.left-parenthesis.]]{1}(.*)[[.right-parenthesis.]]{1})"
                oexpL = "(([\(]{1})([^\)]*)|([\<])([^\>]*))"
                oexpR = "(([^<)]*)([\>]{1})|([^\)]*)([\)]{1}))"
                exp1 = "CONCAT('%s',LOWER('%s'),'%s')" % (expL, search_text_no_plus, expR)
                exp2 = "CONCAT('%s',%s,'%s')" % (expL,"LOWER(wtText)",expR)
                oexp1 = "CONCAT('%s',LOWER('%s'),'%s')" % (oexpL, search_text_no_plus, oexpR)
                oexp2 = "CONCAT('%s',%s,'%s')" % (oexpL, "LOWER(wtText)", oexpR)
                sql_add = "UNION \
                    SELECT \
                    W__.id AS wId, \
                    W__.text AS wText, \
                    wtId, \
                    wtText, \
                    'z_others' AS tText, \
                    '10' AS type, \
                    wcText, \
                    header, \
                    0 AS numOfExamples \
                    FROM \
                    ( \
                    SELECT W.id, W.text,  \
                    GROUP_CONCAT(DISTINCT IF(LOCATE('Artikel',GH.text),GH.text,NULL) SEPARATOR ';') AS header, \
                    GROUP_CONCAT(DISTINCT WC.title ORDER BY WC.title ASC SEPARATOR ';') AS wcText, \
                    GROUP_CONCAT(DISTINCT WT.text ORDER BY WT.text ASC SEPARATOR ',') AS wtTextF \
                    FROM  \
                    ( \
                    SELECT W.id, W.text FROM Word AS W WHERE W.lang in (%(lang)s,2) AND MATCH (W.text) AGAINST('%(text)s' IN BOOLEAN MODE) \
                    ) AS W \
                    INNER JOIN WordTranslate AS WT ON WT.wordId=W.id \
                    LEFT JOIN Grammar AS G ON G.wordId=W.id \
                    LEFT JOIN WordClass AS WC ON WC.id=G.wordClassId \
                    LEFT JOIN WordTranslateType AS WTT ON WTT.wordTranslateId=WT.id \
                    LEFT JOIN GrammarHeader AS GH ON GH.grammarId=G.id \
                    LEFT JOIN Type AS T ON T.id=WTT.typeId \
                    WHERE W.text='%(search_text_no_plus)s' %(fields)s %(wclass)s \
                    GROUP BY W.id \
                    ) AS W__, \
                    ( \
                    SELECT W.id AS wtId, W.text as wtText  \
                    FROM \
                    ( \
                    SELECT WT.wordId AS wtWordId FROM WordTranslate AS WT WHERE LOWER(WT.text) REGEXP %(exp1)s AND LOWER(WT.text) NOT REGEXP %(oexp1)s \
                    ) AS WT_ \
                    INNER JOIN Word AS W ON W.id=wtWordId \
                    GROUP BY wtId \
                    ) AS WT__ \
                    WHERE LOWER(wtTextF) NOT REGEXP %(exp2)s AND LOWER(wtTextF) NOT REGEXP %(oexp2)s \
                " % {'lang':lang, 'text':text, 'fields':fields, 'wclass':wclass, 'exp1':exp1, 'exp2':exp2, 'oexp1':oexp1, 'oexp2':oexp2, 'search_text_no_plus':search_text_no_plus}
            
            column_one = "\
                W.id AS wId, \
                W.text AS wText, \
                WT.id AS wtId, \
                WT.text AS wtText, \
                IF(T.text IS NOT NULL,IF(W.text='%(text)s', T.text, 'z_zothers'), IF(W.text='%(text)s', 'z_others', 'z_zothers')) AS tText, \
                '1' AS type, \
                GROUP_CONCAT(DISTINCT WC.title ORDER BY WC.title ASC SEPARATOR ';') AS wcText, \
                GROUP_CONCAT(DISTINCT IF(LOCATE('Artikel',GH.text),GH.text,NULL) SEPARATOR ';') AS header, \
                SUM(IF(ET.text<>'' AND ET.text IS NOT NULL AND E.text<>'' AND E.text IS NOT NULL,1,0)) AS numOfExamples \
            " % {'text':search_text_no_plus}
            column_two = """
            """
            sql = "\
                SELECT \
                %(column_one)s \
                FROM \
                (((SELECT W.id, W.text FROM Word AS W WHERE W.lang in (%(lang)s,2) AND MATCH (W.text) AGAINST('+%(text)s' IN BOOLEAN MODE)) AS W INNER JOIN WordTranslate AS WT ON WT.wordId=W.id) LEFT JOIN (WordClass AS WC INNER JOIN (Grammar AS G INNER JOIN GrammarHeader as GH ON G.id = GH.grammarId) ON G.wordClassId=WC.id) ON G.wordId=W.id) LEFT JOIN (WordTranslateType AS WTT INNER JOIN (Type AS T INNER JOIN Branche AS B ON T.brancheId = 1) ON T.id=WTT.typeId) ON WTT.wordTranslateId=WT.id LEFT JOIN Example AS E ON E.wordTranslateId=WT.id LEFT JOIN ExampleTranslate AS ET ON ET.exampleId=E.id \
                WHERE 1 %(fields)s %(wclass)s  \
                GROUP BY W.id, WT.text, T.text \
                %(sql_add)s \
                ORDER BY CONVERT(tText USING utf8) COLLATE utf8_general_ci, CONVERT(wText USING utf8) COLLATE utf8_general_ci, type, wtId -- LIMIT 0,1000 \
            " % {'column_one':column_one, 'column_two':column_two, 'lang':lang, 'text':text, 'fields':fields, 'wclass':wclass, 'sql_add':sql_add}
        
        else: #Vietnamese
            print "search vietnamese"
            if (len(search_text_no_plus) > 1):
                expL = "(^|[0-9]{1}[.]{1}[ ]{0,2}|[,]{1}[ ]{0,2}|[[.less-than-sign.]]{1}(.*)[[.greater-than-sign.]]{1}[ ]{0,2})"
                expR = "($|[,][ ]{0,2}|[ ]{0,2}[[.left-parenthesis.]]{1}(.*)[[.right-parenthesis.]]{1})"
                oexpL = "(([\(]{1})([^\)]*)|([\<])([^\>]*))"
                oexpR = "(([^<)]*)([\>]{1})|([^\)]*)([\)]{1}))"
                exp1 = "CONCAT('%s',LOWER('%s'),'%s')" % (expL, search_text_no_plus, expR)
                exp2 = "CONCAT('%s',%s,'%s')" % (expL,"LOWER(wtText)",expR)
                oexp1 = "CONCAT('%s',LOWER('%s'),'%s')" % (oexpL, search_text_no_plus, oexpR)
                oexp2 = "CONCAT('%s',%s,'%s')" % (oexpL, "LOWER(wtText)", oexpR)
                sql_add = "UNION \
                    SELECT \
                    W__.id AS wId, \
                    W__.text AS wText, \
                    wtId, \
                    wtText, \
                    'z_others' AS tText, \
                    '10' AS type, \
                    0 AS numOfExamples \
                    FROM \
                    ( \
                    SELECT W.id, W.text,  \
                    GROUP_CONCAT(DISTINCT WT.text ORDER BY WT.text ASC SEPARATOR ',') AS wtTextF \
                    FROM  \
                    ( \
                    SELECT W.id, W.text FROM Word AS W WHERE W.lang in (%(lang)s,2) AND MATCH (W.text) AGAINST('%(text)s' IN BOOLEAN MODE) \
                    ) AS W \
                    INNER JOIN WordTranslate AS WT ON WT.wordId=W.id \
                    LEFT JOIN WordTranslateType AS WTT ON WTT.wordTranslateId=WT.id \
                    LEFT JOIN Type AS T ON T.id=WTT.typeId \
                    WHERE W.text='%(search_text_no_plus)s' %(fields)s  \
                    GROUP BY W.id \
                    ) AS W__, \
                    ( \
                    SELECT W.id AS wtId, W.text as wtText  \
                    FROM \
                    ( \
                    SELECT WT.wordId AS wtWordId FROM WordTranslate AS WT WHERE LOWER(WT.text) REGEXP %(exp1)s AND LOWER(WT.text) NOT REGEXP %(oexp1)s \
                    ) AS WT_ \
                    INNER JOIN Word AS W ON W.id=wtWordId \
                    GROUP BY wtId \
                    ) AS WT__ \
                    WHERE LOWER(wtTextF) NOT REGEXP %(exp2)s AND LOWER(wtTextF) NOT REGEXP %(oexp2)s \
                " % {'lang':lang, 'text':text, 'fields':fields, 'exp1':exp1, 'exp2':exp2, 'oexp1':oexp1, 'oexp2':oexp2, 'search_text_no_plus':search_text_no_plus}
            
            column_one = "\
                W.id AS wId, \
                W.text AS wText, \
                WT.id AS wtId, \
                WT.text AS wtText, \
                IF(T.text IS NOT NULL,IF(W.text='%(text)s', T.text, 'z_zothers'), IF(W.text='%(text)s', 'z_others', 'z_zothers')) AS tText, \
                '1' AS type, \
                SUM(IF(ET.text<>'' AND ET.text IS NOT NULL AND E.text<>'' AND E.text IS NOT NULL,1,0)) AS numOfExamples \
            " % {'text':search_text_no_plus}
            column_two = """
            """
            sql = "\
                SELECT \
                %(column_one)s \
                FROM \
                (((SELECT W.id, W.text FROM Word AS W WHERE W.lang in (%(lang)s,2) AND MATCH (W.text) AGAINST('+%(text)s' IN BOOLEAN MODE)) AS W INNER JOIN WordTranslate AS WT ON WT.wordId=W.id) LEFT JOIN (WordClass AS WC INNER JOIN (Grammar AS G INNER JOIN GrammarHeader as GH ON G.id = GH.grammarId) ON G.wordClassId=WC.id) ON G.wordId=W.id) LEFT JOIN (WordTranslateType AS WTT INNER JOIN (Type AS T INNER JOIN Branche AS B ON T.brancheId = 1) ON T.id=WTT.typeId) ON WTT.wordTranslateId=WT.id LEFT JOIN Example AS E ON E.wordTranslateId=WT.id LEFT JOIN ExampleTranslate AS ET ON ET.exampleId=E.id \
                WHERE 1 %(fields)s  \
                GROUP BY W.id, WT.text, T.text \
                %(sql_add)s \
                ORDER BY CONVERT(tText USING cp1250), type, CONVERT(wText USING cp1250) LIMIT 0,1000 \
            " % {'column_one':column_one, 'column_two':column_two,'lang':lang, 'text':text, 'fields':fields, 'sql_add':sql_add}
        
        return sql
    
    @staticmethod
    def queryAlgorithm2(text, branche='', wclass='', lang=1):
        fields = ''
        if branche:
            fields = 'AND T.id IN (%s) ' % branche
            
        if wclass: 
            wclass = 'AND WC.id IN (%s) ' % wclass
            
        search_text_no_plus = re.sub('[+]', '', text)
        sql = ''
        if lang == 1:
            sql = "SELECT \
                    wId, \
                    wText, \
                    WT.id AS wtId, \
                    WT.text AS wtText, \
                    'z_others' AS tText, \
                    '1' AS type, \
                    GROUP_CONCAT(DISTINCT WC.title ORDER BY WC.title ASC SEPARATOR ';') AS wcText, \
                    GROUP_CONCAT(DISTINCT IF(LOCATE('Artikel',GH.text),GH.text,NULL) SEPARATOR ';') AS header, \
                    SUM(IF(ET.text<>'' AND ET.text IS NOT NULL AND E.text<>'' AND E.text IS NOT NULL,1,0)) AS numOfExamples \
                    FROM \
                    (SELECT \
                    W.id AS wId, \
                    W.text AS wText, \
                    G.id AS gId, \
                    G.wordClassId AS gWordClassId, \
                    100-levenshtein_ratio(W.text, '%(text)s') AS wScore \
                    FROM (SELECT * FROM GrammarDetails AS GD WHERE GD.text like '%(text)s' ) AS GD_ \
                    INNER JOIN Grammar AS G ON G.id=GD_.grammarId \
                    INNER JOIN Word AS W ON W.id=G.wordId \
                    ORDER BY wScore \
                    LIMIT 0,5) AS GD__ \
                    INNER JOIN GrammarHeader AS GH ON GH.grammarId=GD__.gId \
                    INNER JOIN WordClass AS WC ON WC.id=gWordClassId \
                    INNER JOIN WordTranslate AS WT ON WT.wordId=wId \
                    LEFT JOIN WordTranslateType AS WTT ON WTT.wordTranslateId=WT.id \
                    LEFT JOIN Type AS T ON T.id=WTT.typeId \
                    LEFT JOIN Example AS E ON E.wordTranslateId=WT.id \
                    LEFT JOIN ExampleTranslate AS ET ON ET.exampleId=E.id \
                    WHERE 1 %(fields)s %(wclass)s\
                    GROUP BY wId, WT.text, T.text \
                    ORDER BY tText COLLATE utf8_bin, convert(wText using latin1) collate latin1_general_cs, type, wtId \
            " % {'fields':fields, 'wclass':wclass, 'text':MySQLdb.escape_string('%%"'+search_text_no_plus+'"%%')}
        
        return sql
    
    @staticmethod
    def queryAlgorithm3(text, branche='', wclass='', lang=1):
        if lang==1: lang = 0
        else: lang = 1
        
        fields = ''
        if branche:
            fields = 'AND T.id IN (%s) ' % branche
            
        if wclass: 
            wclass = 'AND WC.id IN (%s) ' % wclass
            
        search_text_no_plus = re.sub('[+]', '', text)
        
        expL = "(^|[0-9]{1}[.]{1}[ ]{0,2}|[,]{1}[ ]{0,2}|[[.less-than-sign.]]{1}(.*)[[.greater-than-sign.]]{1}[ ]{0,2})"
        expR = "($|[,][ ]{0,2}|[ ]{0,2}[[.left-parenthesis.]]{1}(.*)[[.right-parenthesis.]]{1})"
        oexpL = "(([\(]{1})([^\)]*)|([\<])([^\>]*))"
        oexpR = "(([^<)]*)([\>]{1})|([^\)]*)([\)]{1}))"
        exp1 = "CONCAT('%s',LOWER('%s'),'%s')" % (expL, search_text_no_plus, expR)
        oexp1 = "CONCAT('%s',LOWER('%s'),'%s')" % (oexpL, search_text_no_plus, oexpR)
        
        sql = "SELECT \
            W.id AS wid, \
            '%(search_text_no_plus)s' AS wText, \
            WT_.id AS wtId, \
            W.text AS wtText, \
            '10' AS type, \
            '' AS wcText, \
            '' AS header, \
            0 AS numOfExamples, \
            '' AS tText \
            FROM \
            ( \
            SELECT * FROM WordTranslate AS WT WHERE 1 \
            AND LOWER(WT.text) REGEXP %(exp1)s  \
            AND LOWER(WT.text) NOT REGEXP %(oexp1)s \
            ) AS WT_ \
            INNER JOIN Word AS W ON W.id=WT_.wordId \
            WHERE W.lang in (%(lang)s,2) \
            ORDER BY type, CONVERT(wText USING cp1250) LIMIT 0,1000 \
        " % {'search_text_no_plus':search_text_no_plus, 'exp1':exp1, 'oexp1':oexp1, 'lang':lang}
        
        return sql
    
    @staticmethod
    def queryAlgorithm4(text, branche='', wclass='', lang=1):
        fields = ''
        if branche:
            fields = 'AND T.id IN (%s) ' % branche
            
        if wclass: 
            wclass = 'AND WC.id IN (%s) ' % wclass
            
        column_one = ""
        if lang == 1:
            column_two = "W.id AS wId, E.text AS wText, ET.id AS wtId, ET.text AS wtText, WC.title AS wcText, IF(T.text IS NOT NULL,'z_others','z_others') AS tText,  '' as header, '2' AS type, 0 AS numOfExamples "
            sql = "\
                SELECT %(column_two)s \
                FROM \
                ((((Example AS E INNER JOIN WordTranslate AS WT ON (WT.id=E.wordTranslateId AND MATCH(E.text) AGAINST('+%(text)s' IN BOOLEAN MODE))) \
                INNER JOIN Word AS W ON (W.id=WT.wordId AND W.lang in (%(lang)s,2))) \
                INNER JOIN ExampleTranslate AS ET ON ET.exampleId=E.id) \
                LEFT JOIN (WordClass AS WC INNER JOIN (Grammar AS G INNER JOIN GrammarHeader as GH ON G.id = GH.grammarId) ON G.wordClassId=WC.id) ON G.wordId=W.id) \
                LEFT JOIN (ExampleTranslateType AS ETT INNER JOIN Type AS T ON T.id=ETT.typeId INNER JOIN Branche AS B ON T.brancheId = 1) ON ETT.exampleTranslateId=ET.id \
                WHERE 1 %(fields)s %(wclass)s \
                GROUP BY W.id, WT.text, T.text \
                ORDER BY CONVERT(tText USING cp1250), type, CONVERT(wText USING cp1250) LIMIT 0,50 \
                -- ORDER BY wText LIMIT 0,50 \
                " % {'column_one':column_one, 'column_two':column_two,'lang':lang, 'text':text, 'fields':fields, 'wclass':wclass}
        else: 
            column_two = "W.id AS wId, E.text AS wText, ET.id AS wtId, ET.text AS wtText, IF(T.text IS NOT NULL,'z_others','z_others') AS tText,  '' as header, '2' AS type, 0 AS numOfExamples "
            sql = "\
                SELECT %(column_two)s \
                FROM \
                ((((Example AS E INNER JOIN WordTranslate AS WT ON (WT.id=E.wordTranslateId AND MATCH(E.text) AGAINST('+%(text)s' IN BOOLEAN MODE))) \
                INNER JOIN Word AS W ON (W.id=WT.wordId AND W.lang in (%(lang)s,2))) \
                INNER JOIN ExampleTranslate AS ET ON ET.exampleId=E.id) \
                LEFT JOIN (WordClass AS WC INNER JOIN (Grammar AS G INNER JOIN GrammarHeader as GH ON G.id = GH.grammarId) ON G.wordClassId=WC.id) ON G.wordId=W.id) \
                LEFT JOIN (ExampleTranslateType AS ETT INNER JOIN Type AS T ON T.id=ETT.typeId INNER JOIN Branche AS B ON T.brancheId = 1) ON ETT.exampleTranslateId=ET.id \
                WHERE 1 %(fields)s \
                GROUP BY W.id, WT.text, T.text \
                ORDER BY CONVERT(tText USING cp1250), type, CONVERT(wText USING cp1250) LIMIT 0,1000 \
                " % {'column_one':column_one, 'column_two':column_two,'lang':lang, 'text':text, 'fields':fields}
                
        return sql
    
    @staticmethod
    def queryAlgorithm5(text, branche='', wclass='', lang=1):
        fields = ''
        if branche:
            fields = 'AND T.id IN (%s) ' % branche
            
        if wclass: 
            wclass = 'AND WC.id IN (%s) ' % wclass
            
        search_text_no_plus = re.sub('[+]', '', text)
        if lang == 1:
            mapping_arr = {'ÃƒÂ¶l':'Ãƒâ€“l.'}
            search_text_no_plus = mapping_arr.get(search_text_no_plus, search_text_no_plus)
            column = "wScore, \
                W.id AS wId, \
                W.text AS wText, \
                WT.id AS wtId, \
                WT.text AS wtText, \
                'z_zzothers' AS tText, \
                '1' AS type, \
                GROUP_CONCAT(DISTINCT WC.title ORDER BY WC.title ASC SEPARATOR ';') AS wcText, \
                GROUP_CONCAT(DISTINCT IF(LOCATE('Artikel',GH.text),GH.text,NULL) SEPARATOR ';') AS header, \
                SUM(IF(ET.text<>'' AND ET.text IS NOT NULL AND E.text<>'' AND E.text IS NOT NULL,1,0)) AS numOfExamples \
            "
            sql = "\
                SELECT \
                %(column)s \
                FROM \
                (((SELECT *, 100-levenshtein_ratio(text, '%(text)s') AS wScore FROM Word WHERE lang in (%(lang)s,2) AND SOUNDEX(text) = SOUNDEX('%(text)s')) AS W INNER JOIN WordTranslate AS WT ON WT.wordId=W.id) LEFT JOIN (WordClass AS WC INNER JOIN (Grammar AS G INNER JOIN GrammarHeader as GH ON G.id = GH.grammarId) ON G.wordClassId=WC.id) ON G.wordId=W.id) LEFT JOIN (WordTranslateType AS WTT INNER JOIN (Type AS T INNER JOIN Branche AS B ON T.brancheId = 1) ON T.id=WTT.typeId) ON WTT.wordTranslateId=WT.id LEFT JOIN Example AS E ON E.wordTranslateId=WT.id LEFT JOIN ExampleTranslate AS ET ON ET.exampleId=E.id \
                WHERE 1 %(fields)s %(wclass)s  \
                GROUP BY W.id, WT.text, T.text \
                ORDER BY wScore, CONVERT(tText USING cp1250), type, CONVERT(wText USING cp1250) LIMIT 0,50 \
            " % {'column':column, 'lang':lang, 'text':search_text_no_plus, 'fields':fields, 'wclass':wclass}
        
        else:
            column = "wScore, \
                W.id AS wId, \
                W.text AS wText, \
                WT.id AS wtId, \
                WT.text AS wtText, \
                'z_zzothers' AS tText, \
                '1' AS type, \
                SUM(IF(ET.text<>'' AND ET.text IS NOT NULL AND E.text<>'' AND E.text IS NOT NULL,1,0)) AS numOfExamples \
            "
            sql = "\
                SELECT \
                %(column)s \
                FROM \
                (((SELECT *, 100-levenshtein_ratio(text, '%(text)s') AS wScore FROM Word WHERE lang in (%(lang)s,2) AND SOUNDEX(text) = SOUNDEX('%(text)s')) AS W INNER JOIN WordTranslate AS WT ON WT.wordId=W.id) ) LEFT JOIN (WordTranslateType AS WTT INNER JOIN (Type AS T INNER JOIN Branche AS B ON T.brancheId = 1) ON T.id=WTT.typeId) ON WTT.wordTranslateId=WT.id LEFT JOIN Example AS E ON E.wordTranslateId=WT.id LEFT JOIN ExampleTranslate AS ET ON ET.exampleId=E.id \
                WHERE 1 %(fields)s  \
                GROUP BY W.id, WT.text, T.text \
                ORDER BY wScore, CONVERT(tText USING cp1250), type, CONVERT(wText USING cp1250) LIMIT 0,50 \
            " % {'column':column, 'lang':lang, 'text':search_text_no_plus, 'fields':fields}
        
        return sql
    
    @staticmethod
    def getExampleDetail(wordId):
        sql = "select *\
                from (select WT.id as wtId, WT.text as wtText, \
                W.text as wText, E.text as eText, ET.text as etText, T.text as tText \
                from Word as W \
                inner join WordTranslate as WT on WT.wordId = W.id \
                left join Example as E on E.wordTranslateId = WT.id \
                left join ExampleTranslate as ET on ET.exampleId = E.id \
                left join WordTranslateType as WTT on WTT.wordTranslateId = WT.id \
                left join Type as T on WTT.typeId = T.id \
                where W.id =  %(wordId)s \
                order by wtId, CONVERT(eText USING utf8) COLLATE utf8_general_ci) as Hoang\
            " % {'wordId': wordId}
        return sql
    
    @staticmethod
    def getGrammarDetail(wordId):
        sql = "select @rownum := @rownum + 1 AS row_number, \
                WC.title as wcTitle, GH.text as ghText\
                from Grammar G \
                inner join GrammarHeader GH on G.id = GH.grammarId \
                inner join WordClass WC on G.wordClassId = WC.id \
                join (SELECT @rownum := 0) r \
                where G.wordId = %(wordId)s \
            " % {'wordId': wordId}
        return sql
    
    @staticmethod
    def addItem(word, field, lang, status):
        sql = "INSERT INTO SearchHistory(text, typeId, lang, status) \
                VALUES('%s', '%s', %s, %s) \
              " % (word, field, lang, status)
        return sql
    @staticmethod
    def listOne(lang):
        a = "'"
        b = '"'
        sql = "select DISTINCT left(text,1) as data1 from `Word`\
        where lang ='%s' \
        and left(text,1) not in('â€¦','Î©','Î´','(','[','5','1','Å’','$','2','Ã¹','Âº','.','Â£','`','Âµ','Î±','Î²','Ã�','Ã ','Ã‰','Î¼','0',%s%s%s)\
        ORDER BY left(text,1) ASC " % (lang.encode('utf-8'),b,a,b)
        print 'asasas',sql
        return sql
    @staticmethod
    def listTow(lang):
        a = "'"
        b = '"'
        sql = "select DISTINCT left(text,2) as data1 from `Word`\
        where lang ='%s' \
        and left(text,1) not in('â€¦','Î©','Î´','(','[','5','1','Å’','$','2','Ã¹','Âº','.','Â£','`','Âµ','Î±','Î²','Ã�','Ã ','Ã‰','Î¼','0',%s%s%s)\
        ORDER BY left(text,2) ASC " % (lang.encode('utf-8'),b,a,b)
        print 'asasas',sql
        return sql
    @staticmethod
    def listThree(lang):
        a = "'"
        b = '"'
        sql = "select DISTINCT left(text,3) as data1 from `Word`\
        where lang ='%s' \
        and left(text,1) not in('â€¦','Î©','Î´','(','[','5','1','Å’','$','2','Ã¹','Âº','.','Â£','`','Âµ','Î±','Î²','Ã�','Ã ','Ã‰','Î¼','0',%s%s%s)\
        ORDER BY left(text,3) ASC " % (lang.encode('utf-8'),b,a,b)
        print 'asasas',sql
        return sql
    @staticmethod
    def detail(vingu,lang):
        a = '%'
        print "Vi Ngu ",vingu.encode('utf-8')
        sql ="select W.text , W.id ,G.text as nghia ,T.`text` as loai \
            from `Word` W \
            left join WordTranslate G on G.`wordId` = W.`id`\
            left join WordTranslateType GT on GT.`wordTranslateId` = G.`id`\
            left join `Type` T on T.`id` = GT.`typeId`\
            where W.text like '%s%s' \
            and lang ='%s' \
            and LENGTH(W.text) >2  limit 100" % (smart_unicode(vingu),a,lang)
        print "Detail ",sql.encode('utf-8')
        return sql
    @staticmethod
    def detailWord(vingu,lang,linhvuc):
        a = '%'
        print "Vi Ngu ",vingu.encode('utf-8')
        sql ="select W.text , W.id ,G.text as nghia ,T.`text` as loai \
            from `Word` W \
            left join WordTranslate G on G.`wordId` = W.`id`\
            left join WordTranslateType GT on GT.`wordTranslateId` = G.`id`\
            left join `Type` T on T.`id` = GT.`typeId`\
            where W.text like '%s%s' \
            and lang ='%s' \
            and T.id  IN (%s) \
            and LENGTH(W.text) >2  limit 500" % (smart_unicode(vingu),a,lang,linhvuc)
        print "Detail ",sql.encode('utf-8')
        return sql
    
        
class TTSearchEngine():
    def __str__(self):
        return u'Search Provider Class'
        
    @staticmethod
    def search(text, branche='', wclass='', lang=1):
        text = MySQLdb.escape_string(text.encode('utf8'))
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.queryAlgorithm1(text, branche=branche, wclass=wclass, lang=lang)
        print "Query first time"
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        if len(result) == 0:
            queryString = TTSearchProvider.queryAlgorithm2(text, branche=branche, wclass=wclass, lang=lang)
            print "-"*100
            print "Query second time"
            if queryString != '':
                cursor.execute(queryString)
                result = dictFetchAll(cursor)
            if len(result) == 0:
                queryString = TTSearchProvider.queryAlgorithm3(text, branche=branche, wclass=wclass, lang=lang)
                print "-"*100
                print "Query third time"
                if queryString != '':
                    cursor.execute(queryString)
                    result = dictFetchAll(cursor)
                if len(result) == 0:
                    queryString = TTSearchProvider.queryAlgorithm4(text, branche=branche, wclass=wclass, lang=lang)
                    print "-"*100
                    print "Query fourth time"
                    if queryString != '':
                        cursor.execute(queryString)
                        result = dictFetchAll(cursor)
                    if len(result) == 0:
                        queryString = TTSearchProvider.queryAlgorithm5(text, branche=branche, wclass=wclass, lang=lang)
                        print "-"*100
                        print "Query fifth time"
                        if queryString != '':
                            cursor.execute(queryString)
                            result = dictFetchAll(cursor)
                        
        #for r in result:
        #    print "%s: %s" % (r["wText"] ,r["wtText"])
            
        #print "-"*100
        if len(result) != 0:
            addItemSearch(word=text, field='', lang=lang, status=1)
        return result
        
    def __writeHistory(self):
        pass
    
    @staticmethod
    def exampleDetail( wordId):
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.getExampleDetail( wordId)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result
    
    @staticmethod
    def grammarDetail(wordId):
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.getGrammarDetail(wordId)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result
    @staticmethod
    def listOne(lang):
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.listOne(lang)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result
    @staticmethod
    def listTow(lang):
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.listTow(lang)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result
    @staticmethod
    def listThree(lang):
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.listThree(lang)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result
    @staticmethod
    def detail(vingu,lang):
        
        result = []
        #vingu = MySQLdb.escape_string(vingu.encode('utf8'))
        cursor = connection.cursor()
        queryString = TTSearchProvider.detail(smart_unicode(vingu),lang)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result
    @staticmethod
    def detailWord(vingu,lang,linhvuc):
         
        result = []
        #vingu = MySQLdb.escape_string(vingu.encode('utf8'))
        cursor = connection.cursor()
        queryString = TTSearchProvider.detailWord(smart_unicode(vingu),lang,linhvuc)
        if queryString != '':
            cursor.execute(queryString)
            result = dictFetchAll(cursor)
        return result      

class SearchTest():
    def __str__(self):
        return u'Search Provider Class'
        
    @staticmethod
    def search1(self, text, branche='', wclass='', lang=1):
        text = MySQLdb.escape_string(text)
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.queryAlgorithm1(text, branche=branche, wclass=wclass, lang=lang)
        cursor.execute(queryString)
        result = dictFetchAll(cursor)
        return result
    
    @staticmethod
    def search2(self, text, branche='', wclass='', lang=1):
        text = MySQLdb.escape_string(text)
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.queryAlgorithm2(text, branche=branche, wclass=wclass, lang=lang)
        cursor.execute(queryString)
        result = dictFetchAll(cursor)
        return result
    
    @staticmethod
    def search3(self, text, branche='', wclass='', lang=1):
        text = MySQLdb.escape_string(text)
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.queryAlgorithm3(text, branche=branche, wclass=wclass, lang=lang)
        cursor.execute(queryString)
        result = dictFetchAll(cursor)
        return result
    
    @staticmethod
    def search4(self, text, branche='', wclass='', lang=1):
        text = MySQLdb.escape_string(text)
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.queryAlgorithm4(text, branche=branche, wclass=wclass, lang=lang)
        cursor.execute(queryString)
        result = dictFetchAll(cursor)
        return result
    
    @staticmethod
    def search5(self, text, branche='', wclass='', lang=1):
        text = MySQLdb.escape_string(text)
        result = []
        cursor = connection.cursor()
        queryString = TTSearchProvider.queryAlgorithm5(text, branche=branche, wclass=wclass, lang=lang)
        cursor.execute(queryString)
        result = dictFetchAll(cursor)
        return result
        
    def __writeHistory(self):
        pass
    

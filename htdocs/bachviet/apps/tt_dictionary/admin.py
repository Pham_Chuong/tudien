from django.contrib import admin
from bachviet.apps.tt_dictionary.models import *

admin.site.register(Branche)
admin.site.register(Example)
admin.site.register(ExampleTranslate)
admin.site.register(ExampleTranslateType)
admin.site.register(Grammar)
admin.site.register(GrammarDetails)
admin.site.register(GrammarHeader)
admin.site.register(Media)
admin.site.register(SearchHistory)
admin.site.register(Type)
admin.site.register(Word)
admin.site.register(WordClass)
admin.site.register(WordTranslate)
admin.site.register(WordTranslateType)

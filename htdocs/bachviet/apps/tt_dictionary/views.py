# -*- coding: utf8 -*-
from django.http import HttpResponse, JsonResponse
from django.utils.encoding import smart_unicode
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.core.context_processors import csrf
from django.template.loader import render_to_string
import json
from django.views.decorators.csrf import csrf_protect
from django.core import serializers
import decimal
from bachviet.apps.tt_dictionary.models import Word, TTSearchEngine, SearchTest, Type, WordClass
from bachviet.apps.tt_management.views import getAnalytics
from django.core.exceptions import ValidationError

def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError

@csrf_protect
def index(request):
    word = request.POST.get('word')
    lang = request.POST.get('lang')
    query = ''
    if word:
        text = TTSearchEngine()
        query = text.search(text=word,lang=int(lang))
    lenght = len(query)
    print len(query)
    return render(request, 'index.html', {'query': query, 'word': word, 'lang': lang, 
                                          'lenght': lenght})
    
@csrf_protect
def test(request):
    word = request.POST.get('word')
    lang = request.POST.get('lang')
    query = ''
    if word:
        text = TTSearchEngine()
        query = text.search(text=word,lang=int(lang))
        json_input = query
        json_input = json.loads(json.dumps(json_input))
        
    return HttpResponse(json_input, content_type='application/json')

@csrf_protect
def test1(request):
    text = request.GET.get('text')
    word = str(text.split('=')[2].encode('utf-8'))
    wordId = int(text.split('=')[1].split('&')[0])
    detail = {}
    type_word = ''
    test = TTSearchEngine()
    detail = test.exampleDetail(wordId=wordId)
    grammardetail = test.grammarDetail(wordId=wordId)
    grammar = []
    for q in grammardetail:
        grammar.append(int(q['row_number']))
        type_word = q['wcTitle']
        grammar.append(type_word)
        tmp = json.loads(q['ghText'])
        for kt in range(len(tmp)):
            text_grammar = {}
            text_grammar['name'] = tmp[kt]['_name']+':'
            text_grammar['text'] = tmp[kt]['_text']
            grammar.append(tmp[kt]['_name']+':')
            grammar.append(tmp[kt]['_text'])
    return render(request, 'detail.html', {'detail': detail, 'grammar': grammar, 'type_word': type_word})


def words(request):
    query = request.GET.get("q") and request.GET.get("q") or ""
    limit = request.GET.get("limit") and request.GET.get("limit") or 10
    list_words = Word.objects.filter(Q(lang=1) | Q(lang=2), text__startswith=(query.encode('utf8')))[:limit]
    
    result = []
    for word in list_words:
        #print "sasasasasasa",word
        result.append(word.text)
        
    return HttpResponse(json.dumps(result), content_type='application/json')
    
def search(request):
    keyword = request.GET.get("k")
    language = request.GET.get("language")
    result = TTSearchEngine.search(keyword, lang=language)
    print "--debug --------",result
    return HttpResponse(result)

@csrf_protect
def detail2(request):
    text = request.POST.get('list')
    lang = request.POST.get('lang')
    print "text ",text.encode('utf-8')
    print "lang ",lang
    data = []
    if text:
        datasearch = TTSearchEngine()
        data = datasearch.detail(text.encode('utf-8','ignore'),lang)
    word = HttpResponse(json.dumps(data), content_type='application/json')
    request.session['resultword'] = data
    print "Liet ke tu",data
    return word 
@csrf_protect
def listdetail(request):
    query2 = request.session['resultword']
    sum1 = len(query2)
    return render(request, 'layout2.html', {'query2': query2,'sum1': sum1})

@csrf_protect
def result(request):
    word = request.POST.get('word')
    lang = request.POST.get('lang')
    wordId = request.GET.get('wordId')
    query = []
    detail = []
    get_count = 0
    text = TTSearchEngine()
    if word and lang:
        query = text.search(text=word,lang=int(lang))
        get_count = getAnalytics()
           
    sum = len(query)
    option_type = Type.objects.all()
    option_class = WordClass.objects.all()
    if wordId:
        word = request.GET.get('word')
        detail = text.exampleDetail(wordId=wordId)
    print 'Test',detail
    return render(request, 'layout2.html', {'query': query, 'lang': lang, 'word': word,
                                            'get_count': get_count, 'sum': sum, 'detail': detail, 'option_type':option_type, 'option_class':option_class })


@csrf_protect
def list(request):
    list = request.POST.get('list')
    lang = request.POST.get('lang')
    print "Value",list
    print "lang",lang
    query1 = []
    print "Pham Chuong"
    text = TTSearchEngine()
    if (list == '2'):
        query1 = text.listOne(lang)
    elif(list == '3'):
        query1 = text.listTow(lang)
    elif(list == '4'):
        query1 = text.listThree(lang)   
    print query1
    print len(query1)
    return render(request, 'layout2.html', {'query1': query1})



@csrf_protect
def detail1(request):
    text = request.POST.get('text')
    print 'Hoang ', text.encode('utf-8')
    data = []
    if text:
        
        wordId = int(text.split('=')[1].split('&')[0])
        datasearch = TTSearchEngine()
        data = datasearch.exampleDetail(wordId=wordId)
    reponse = HttpResponse(json.dumps(data), content_type='application/json')
    request.session['resultsearch'] = data
    return reponse


@csrf_protect
def detail(request):
    
    data = request.session['resultsearch']

        
    
    get_count = getAnalytics()
    return render(request, 'layout3.html', {'data': data, 'get_count': get_count})


@csrf_protect
def detail3(request):
    text = request.POST.get('list')
    lang = request.POST.get('lang')
    linhvuc = request.POST.get('fiel')
    #linhvuc = (8,10)
    print "text ",text.encode('utf-8')
    print "fielArray ",linhvuc
    data = []
    if text:
        datasearch = TTSearchEngine()
        data = datasearch.detailWord(text.encode('utf-8','ignore'),lang,linhvuc)
    word = HttpResponse(json.dumps(data), content_type='application/json')
    request.session['resultword'] = data
    print "Liet ke tu",data
    return word 
@csrf_protect
def listfield(request):
    query4 = request.session['resultword']
    sum4 = len(query4)
    return render(request, 'layout2.html', {'query4': query4,'sum4':sum4})

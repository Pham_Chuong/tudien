# -*- coding: UTF-8 -*- 

from bachviet.apps.tt_dictionary.models import *
from pyExcelerator import *
import re, logging
from bachviet.settings import LOG_DIR

logger = logging.getLogger("bachviet.debug")
handler = logging.FileHandler(LOG_DIR + "parser.log")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class BaseParser():
    arr_br1 = [
            'điện tử',
            'tôn giáo',
            'sinh lý',
            'hóa học',
            'từ mượn',
            'toán học',
            'thể thao',
            'luyện kim',
            'kỹ thuật',
            'quân sự',
            'vật lý',
            'động vật học',
            'hàng hải',
            'hàng không',
            'báo chí',
            'thần học',
            'sân khấu phim ảnh',
            'thực vật học',
            'chính trị',
            'tâm lý học',
            'kiến trúc',
            'thiên văn học',
            'địa chất',
            'ngành in',
            'nông lâm nghiệp',
            'ngôn ngữ học',
            'kinh tế',
            'khoáng học',
            'hội họa',
            'săn bắn',
            'ngành dệt',
            'dược học',
            'âm nhạc',
            'triết học và kinh điển',
            'dân tộc học',
            'thần thoại',
            'nha khoa',
            'khí tượng',
            'phân tâm học',
            'khoáng chất',
            'xã hội học',
            'âm ngữ học',
            'khoa học',
            'trắc địa học',
            'y học',
            'địa lý',
            'truyền hình',
            'giáo dục',
            'giải phẩu học',
            'gia chánh',
            'văn học',
            'tiền tệ',
            'thống kê học',
            'viễn thông',
            'nghề nghiệp',
            'ngành thú y',
            'vũ trụ học',
            'nghệ thuật',
            'hải dương học',
            'sinh học',
            'ngành kiểm lâm',
            'chiêm tinh học',
            'dinh dưỡng học',
            'thiên vật học',
            'huyền bí học',
            'thần bí học',
            'luân lý học',
            'kế toán',
            'hùng biện',
            'siêu tâm linh học',
            'ngành thư viện',
            'âm học',
            'ứng dụng tin học',
            'chế tạo phi cơ',
            'ngành mộc',
            'khoa khảo cổ',
            'nhiếp ảnh',
            'lý luận học',
            'du lịch',
            'luật',
            'hỏa xa',
            'lịch sử',
            'ngôn ngữ nói',
            'chăn nuôi'
    ]
    
    arr_br3 = [
                'đồng nghĩa',
                'kí hiệu',
                'viết tắt',
                'liên từ',
                'phản nghĩa',
                'từ mượn',
                'nghĩa bóng',
                'phái nữ',
                'phái nam'
    ]
    
    lang = 1
    word = None
    word_translate = None
    example = None
    example_translate = None
    
    def __init__(self, parser_type="duc-viet"):
        if parser_type=="viet-duc":
            self.lang = 0
            
    def writeWordHistory(self, word_id, status=0, update_type='', update_description=''):
        word_history = WordHistory(wordId=word_id, status=status, updateType=update_type, updateDescription=str(update_description))
        word_history.save()
            
    def writeLog(self, text="", level="debug"):
        if level == "debug":
            logger.debug(text)
        elif level == "error":
            logger.error(text)
        else:
            logger.info(text)
        
    def __reset():
        pass
    
    def __process():
        pass
    
    def parse(self):
        pass
    
    def processBranche(self):
        pass
    
    def getBranches(self, text):
        branche_str = re.search('\([^;\)\(]*;[^;\)\(]*;[^;\)\(]*\)', text)
        if branche_str is None:
            return {'str':text, 'branche1':None, 'branche2':None, 'branche3':None}
            
        branche_str = branche_str.group()
        title = re.sub('\([^;\)\(]*;[^;\)\(]*;[^;\)\(]*\)', '', text)
        if (not title) or title == '': title = ''
        branche_arr = re.split("[(;)]", branche_str)
        
        br1 = [item.strip() for item in re.split(',', branche_arr[1]) if item != '-']
        br2 = [item.strip() for item in re.split(',', branche_arr[2]) if item != '-']
        br3 = [item.strip() for item in re.split(',', branche_arr[3]) if item != '-']

        return {'str':title, 'branche1':br1, 'branche2': {'br2': br2, 'br1': br1}, 'branche3': br3}

    def portToTablesType(self, res, brancheId, addIntoTable, id):
        
        arrType ={
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\x83 thao':'thể thao',
            'm\xc3\xb4n th\xe1\xbb\x83 thao ki\xe1\xba\xbfm thu\xe1\xba\xadt':'thể thao',
            'b\xc3\xa0n c\xe1\xbb\x9d t\xc6\xb0\xe1\xbb\x9bng t\xc3\xa2y':'thể thao',
            'b\xc3\xb3n \xc4\x91\xc3\xa1':'thể thao',
            'b\xc3\xb3ng \xc4\x91\xc3\xa1':'thể thao',
            'c\xe1\xbb\x9d vua [c\xe1\xbb\x9d t\xc6\xb0\xe1\xbb\x9bng t\xc3\xa2y]':'thể thao',
            'c\xe1\xbb\xad t\xe1\xba\xa1':'thể thao',
            'm\xc3\xb4n xe \xc4\x91\xe1\xba\xa1p':'thể thao',
            'qu\xe1\xba\xa7n v\xe1\xbb\xa3t':'thể thao',
            'th\xe1\xbb\x83 d\xe1\xbb\xa5c':'thể thao',
            'th\xe1\xbb\x83 thao':'thể thao',
            'th\xe1\xbb\x83 thao \xc4\x91ua xe':'thể thao',
            '\xc4\x91\xc3\xa1nh c\xe1\xbb\x9d':'thể thao',
            'ki\xcc\xa3ch ngh\xc3\xaa\xcc\xa3':'sân khấu phim ảnh',
            'k\xe1\xbb\x8bch':'sân khấu phim ảnh',
            'k\xe1\xbb\x8bch ngh\xe1\xbb\x87':'sân khấu phim ảnh',
            'ng\xc3\xa0nh di\xe1\xbb\x85n vi\xc3\xaan':'sân khấu phim ảnh',
            'phim':'sân khấu phim ảnh',
            'phim \xe1\xba\xa3nh':'sân khấu phim ảnh',
            's\xc3\xa2n kh\xe1\xba\xa5u':'sân khấu phim ảnh',
            's\xc3\xa2n kh\xe1\xba\xa5u k\xe1\xbb\x8bch ngh\xe1\xbb\x87 phim':'sân khấu phim ảnh',
            '\xc4\x91i\xe1\xbb\x87n \xe1\xba\xa3nh':'sân khấu phim ảnh',
            'v\xe1\xbb\x9f k\xe1\xbb\x8bch':'sân khấu phim ảnh',
            '\xc3\xa2m gi\xe1\xbb\x8dng':'âm ngữ học',
            '\xc3\xa2m h\xe1\xbb\x8dc':'âm học',
            '\xc3\xa2m ng\xe1\xbb\xaf h\xe1\xbb\x8dc':'âm ngữ học',
            '\xc3\xa2m nha\xcc\xa3c':'âm nhạc',
            '\xc3\xa2m nh\xe1\xba\xa1c':'âm nhạc',
            'nh\xe1\xba\xa1c c\xe1\xbb\xa5':'âm nhạc',
            '\xc4\x91\xc3\xa0n d\xc6\xb0\xc6\xa1ng c\xe1\xba\xa7m':'âm nhạc',
            'kh\xe1\xba\xa9u c\xe1\xba\xa7m':'âm nhạc',
            'nh\xe1\xba\xa1c':'âm nhạc',
            'b\xc3\xa1o ch\xc3\xad':'báo chí',
            'tin t\xe1\xbb\xa9c':'báo chí',
            'b\xe1\xbb\x87nh l\xc3\xbd h\xe1\xbb\x8dc':'y học',
            'b\xe1\xbb\x87nh l\xc3\xbd y h\xe1\xbb\x8dc':'y học',
            'b\xe1\xbb\x87nh nh\xc3\xa2n':'y học',
            'b\xe1\xbb\x87nh t\xe1\xba\xadt':'y học',
            'b\xe1\xbb\x8b l\xe1\xba\xa1nh ch\xc3\xa2n':'y học',
            'y h\xe1\xbb\x8dc':'y học',
            'y h\xe1\xbb\x8dc-':'y học',
            'y h\xe1\xbb\x8dc':'y học',
            'y hoc':'y học',
            'y khoa':'y học',
            'c\xe1\xba\xa5p c\xe1\xbb\xa9u':'y học',
            'canh n\xc3\xb4ng':'nông lâm nghiệp',
            'ng\xc3\xa0nh canh n\xc3\xb4ng':'nông lâm nghiệp',
            'ng\xc3\xa0nh n\xc3\xb4ng l\xc3\xa2m s\xe1\xba\xa3n':'nông lâm nghiệp',
            'n\xc3\xb4ng nghi\xc3\xaa\xcc\xa3p':'nông lâm nghiệp',
            'n\xc3\xb4ng nghi\xe1\xbb\x87p':'nông lâm nghiệp',
            'ngh\xe1\xbb\x81 n\xc3\xb4ng':'nông lâm nghiệp',
            'l\xc3\xa2m h\xe1\xbb\x8dc':'nông lâm nghiệp',
            'l\xc3\xa2m s\xe1\xba\xa3n':'nông lâm nghiệp',
            'ng\xc3\xa0nh ki\xe1\xbb\x83m l\xc3\xa2m':'ngành kiểm lâm',
            'chi\xc3\xaam tinh h\xe1\xbb\x8dc':'chiêm tinh học',
            'ch\xc3\xadnh tr\xe1\xbb\x8b':'chính trị',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\x99ng s\xe1\xba\xa3n':'chính trị',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\x99ng s\xe1\xba\xa3n \xc4\x90\xc3\xb4ng \xc4\x90\xe1\xbb\xa9c':'chính trị',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\x99ng s\xe1\xba\xa3n \xc4\x90\xc3\xb4ng \xc4\x90\xe1\xbb\xa9c c\xc5\xa9':'chính trị',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\x99ng s\xe1\xba\xa3n \xc4\x90\xe1\xbb\xa9c':'chính trị',
            'c\xc6\xa1 h\xe1\xbb\x8dc':'vật lý',
            '\xc4\x91i\xc3\xaa\xcc\xa3n ho\xcc\xa3c':'vật lý',
            'khoa kh\xc3\xad \xc4\x91\xe1\xbb\x99ng h\xe1\xbb\x8dc':'vật lý',
            'nhi\xe1\xbb\x87t h\xe1\xbb\x8dc':'vật lý',
            'v\xe1\xba\xadt l\xc3\xbd \xc4\x91\xe1\xbb\x8ba d\xc6\xb0 h\xe1\xbb\x8dc':'vật lý',
            'v\xe1\xba\xadt l\xc3\xbd h\xe1\xbb\x8dc':'vật lý',
            'v\xe1\xba\xadt l\xc3\xad':'vật lý',
            'v\xe1\xba\xadt l\xc3\xad h\xe1\xbb\x8dc':'vật lý',
            'v\xe1\xba\xadt l\xc3\xbd':'vật lý',
            'v\xe1\xba\xadt l\xc3\xbd h\xe1\xba\xa1t nh\xc3\xa2n':'vật lý',
            'v\xe1\xba\xadt l\xc3\xbd h\xe1\xba\xa1t nh\xc3\xa2n [v\xe1\xba\xadt l\xc3\xbd h\xe1\xba\xa1ch nh\xc3\xa2n]':'vật lý',
            'v\xe1\xba\xadt l\xc3\xbd h\xe1\xba\xa1t nh\xc3\xa2n [v\xe1\xba\xadt l\xc3\xbd h\xe1\xba\xa1ch t\xc3\xa2m]':'vật lý',
            'v\xc3\xa2\xcc\xa3t ly\xcc\x81 ho\xcc\xa3c':'vật lý',
            'nguy\xc3\xaan t\xe1\xbb\xad h\xe1\xbb\x8dc':'vật lý',
            'kh\xc3\xad \xc4\x91\xe1\xbb\x99ng h\xe1\xbb\x8dc':'vật lý',
            'k\xc3\xadnh quang h\xe1\xbb\x8dc':'vật lý',
            'quan h\xe1\xbb\x8dc':'vật lý',
            'th\xe1\xbb\x9di gian':'vật lý',
            'th\xe1\xbb\x9di gian t\xc3\xadnh':'chính trị',
            'k\xc3\xadnh hi\xe1\xbb\x83n vi':'vật lý',
            '\xc4\x91i\xe1\xbb\x81u ki\xe1\xbb\x87n h\xe1\xbb\x8dc':'vật lý',
            '\xc4\x91i\xe1\xbb\x83m h\xe1\xbb\x8dc':'vật lý',
            '\xc4\x91i\xe1\xbb\x87n':'vật lý',
            '\xc4\x91i\xe1\xbb\x87n hoc':'vật lý',
            '\xc4\x91i\xe1\xbb\x87n h\xe1\xbb\x8dc':'vật lý',
            '\xc4\x91i\xe1\xbb\x87n nh\xe1\xba\xb9':'vật lý',
            '\xc4\x91\xe1\xbb\x8b\xc3\xaan h\xe1\xbb\x8dc':'vật lý',
            'c\xc6\xa1 th\xe1\xbb\x83 gi\xe1\xba\xa3i ph\xe1\xba\xa9u h\xe1\xbb\x8dc':'giải phẫu học',
            'c\xc6\xa1 th\xe1\xbb\x83 gi\xe1\xba\xa3i ph\xe1\xba\xabu h\xe1\xbb\x8dc':'giải phẫu học',
            'c\xc6\xa1 th\xe1\xbb\x83 h\xe1\xbb\x8dc':'giải phẫu học',
            'gi\xe1\xba\xa3i ph\xe1\xba\xabu':'giải phẫu học',
            'gi\xe1\xba\xa3i':'',
            'c\xc3\xb4n tr\xc3\xb9ng':'sinh học',
            'c\xc3\xb4n tr\xc3\xb9ng h\xe1\xbb\x8dc':'sinh học',
            'di truy\xe1\xbb\x81n h\xe1\xbb\x8dc':'sinh học',
            'ng\xc3\xb4n t\xe1\xbb\xab sinh h\xe1\xbb\x8dc':'sinh học',
            'sinh v\xc3\xa2\xcc\xa3t ho\xcc\xa3c':'sinh học',
            'sinh v\xe1\xba\xadt':'sinh học',
            'sinh v\xe1\xba\xadt h\xe1\xbb\x8dc':'sinh học',
            'sinh  v\xe1\xba\xadt h\xe1\xbb\x8dc':'sinh học',
            'sinh h\xe1\xbb\x8dc':'sinh học',
            'vi khu\xe1\xba\xa9n h\xe1\xbb\x8dc':'sinh học',
            'vi tr\xc3\xb9ng h\xe1\xbb\x8dc':'sinh học',
            't\xe1\xba\xbf b\xc3\xa0o':'sinh học',
            'th\xc3\xba v\xe1\xba\xadt c\xc3\xb3 mai c\xe1\xbb\xa9ng':'sinh học',
            'th\xc3\xba v\xc3\xa2t':'sinh học',
            'th\xc3\xba v\xe1\xba\xadt':'sinh học',
            'th\xc3\xba v\xe1\xba\xadt c\xc3\xb2n nh\xe1\xbb\x8f':'sinh học',
            'da-':'sinh học',
            'c\xc3\xa2y c\xe1\xbb\x91i':'sinh học',
            'd\xc3\xa2n t\xe1\xbb\x99c h\xe1\xbb\x8dc':'dân tộc học',
            '\xc4\x91\xe1\xba\xa1o H\xe1\xbb\x93i gi\xc3\xa1o':'tôn giáo',
            '\xc4\x91\xe1\xba\xa1o Tin L\xc3\xa0nh':'tôn giáo',
            '\xc4\x91\xe1\xba\xa1o Tin l\xc3\xa0nh':'tôn giáo',
            'H\xc3\xb4\xcc\x80i gia\xcc\x81o':'tôn giáo',
            'H\xe1\xbb\x93i gi\xc3\xa1o':'tôn giáo',
            '{\xc4\x91\xe1\xba\xa1o} H\xe1\xbb\x93i gi\xc3\xa1o':'tôn giáo',
            'Thi\xc3\xaan Ch\xc3\xbaa Gi\xc3\xa1o':'tôn giáo',
            'Thi\xc3\xaan Ch\xc3\xbaa gi\xc3\xa1o':'tôn giáo',
            'Thi\xc3\xaan Ch\xc3\xbaa gi\xc3\xa1o v\xc3\xa0 Ch\xc3\xadnh th\xe1\xbb\x91ng gi\xc3\xa1o':'tôn giáo',
            'Thi\xc3\xaan chu\xc3\xa1 gi\xc3\xa1o':'tôn giáo',
            'Thi\xc3\xaan ch\xc3\xba gi\xc3\xa1o':'tôn giáo',
            'Thi\xc3\xaan ch\xc3\xbaa gi\xc3\xa1o':'tôn giáo',
            'gi\xc3\xa1o h\xe1\xbb\x99i Tin L\xc3\xa0nh T\xc3\xa2n gi\xc3\xa1o':'tôn giáo',
            'gi\xc3\xa1o s\xc4\xa9 Thi\xc3\xaan Ch\xc3\xbaa gi\xc3\xa1o':'tôn giáo',
            'thi\xc3\xaan ch\xc3\xbaa gi\xc3\xa1o':'tôn giáo',
            '1. t\xc3\xb4n gi\xc3\xa1o':'tôn giáo',
            't\xc3\xb4n gi\xc3\xa1o':'tôn giáo',
            't\xc3\xb4n gi\xc3\xa1o-':'tôn giáo',
            'Tin L\xc3\xa0nh':'tôn giáo',
            'T\xc3\xb4n gi\xc3\xa1o':'tôn giáo',
            'Ph\xe1\xba\xadt gi\xc3\xa1o':'tôn giáo',
            'Do Th\xc3\xa1i gi\xc3\xa1o':'tôn giáo',
            'tu s\xc4\xa9 t\xc3\xb4n gi\xc3\xa1o':'tôn giáo',
            'ng\xc3\xb4n t\xe1\xbb\xab \xc4\x91\xe1\xba\xa1o Thi\xc3\xaan Ch\xc3\xbaa':'tôn giáo',
            'ng\xc3\xb4n t\xe1\xbb\xab gi\xc3\xa1o h\xe1\xbb\x99i':'tôn giáo',
            '\xc4\x91i\xcc\xa3a ch\xc3\xa2\xcc\x81t':'địa chất',
            '\xc4\x91i\xcc\xa3a ch\xc3\xa2\xcc\x81t ho\xcc\xa3c':'địa chất',
            '\xc4\x91i\xe1\xba\xa1 ch\xe1\xba\xa5t':'địa chất',
            '\xc4\x91i\xe1\xba\xa1 ch\xe1\xba\xa5t h\xe1\xbb\x8dc':'địa chất',
            '\xc4\x91\xe1\xbb\x8ba ch\xe1\xba\xa5t':'địa chất',
            '\xc4\x91\xe1\xbb\x8ba ch\xe1\xba\xa5t h\xe1\xbb\x8dc':'địa chất',
            '\xc4\x91\xe1\xbb\x8ba l\xc3\xbd h\xe1\xbb\x8dc':'địa lý',
            '\xc4\x91\xe1\xbb\x8ba v\xe1\xba\xadt l\xc3\xbd h\xe1\xbb\x8dc':'địa lý',
            '\xc4\x91\xe1\xbb\x8ba l\xc3\xad':'địa lý',
            '\xc4\x91\xe1\xbb\x8ba l\xc3\xbd':'địa lý',
            'th\xe1\xbb\x9di ti\xe1\xba\xbft':'địa lý',
            'khoa v\xe1\xba\xbd \xc4\x91\xe1\xbb\x8ba \xc4\x91\xe1\xbb\x93':'địa lý',
            'n\xc6\xa1i ch\xe1\xbb\x91n':'địa lý',
            '\xc4\x91i\xe1\xbb\x87n tho\xe1\xba\xa1i':'viễn thông',
            '\xc4\x91i\xe1\xbb\x87n t\xc3\xadn':'viễn thông',
            '\xc4\x91i\xc3\xaa\xcc\xa3n toa\xcc\x81n':'điện tử',
            '\xc4\x91i\xe1\xbb\x87n to\xc3\xa1n':'điện tử',
            '\xc4\x91i\xe1\xbb\x87n t\xc3\xb3an':'điện tử',
            '\xc4\x91i\xe1\xbb\x87n t\xe1\xbb\xad':'điện tử',
            'm\xc3\xa1y m\xc3\xb3c':'điện tử',
            'dinh d\xc6\xb0\xe1\xbb\xa1ng h\xe1\xbb\x8dc':'dinh dưỡng học',
            'd\xc6\xb0\xc6\xa1\xcc\xa3c ho\xcc\xa3c':'dược học',
            'd\xc6\xb0\xe1\xbb\xa3c h\xe1\xbb\x8dc':'dược học',
            'd\xc6\xb0\xe1\xbb\xa3c khoa':'dược học',
            'gia ch\xc3\xa1nh':'gia chánh',
            'gi\xc3\xa1o d\xe1\xbb\xa5c':'giáo dục',
            'tr\xc6\xb0\xe1\xbb\x9dng h\xe1\xbb\x8dc':'giáo dục',
            'h\xe1\xba\xa7m m\xe1\xbb\x8f':'khoáng học',
            'ng\xc3\xa0nh h\xe1\xba\xa7m m\xe1\xbb\x8f':'khoáng học',
            'ng\xc3\xa0nh d\xe1\xba\xa7u m\xe1\xbb\x8f':'khoáng học',
            'ng\xc3\xa0nh khai m\xe1\xbb\x8f':'khoáng học',
            'ng\xc3\xa0nh khai th\xc3\xa1c m\xe1\xbb\x8f':'khoáng học',
            'khoa\xcc\x81ng ho\xcc\xa3c':'khoáng học',
            'kho\xc3\xa1ng h\xe1\xbb\x8dc':'khoáng học',
            'kho\xc3\xa1ng ch\xe1\xba\xa5t':'khoáng chất',
            'ha\xcc\x80ng ha\xcc\x89i':'hàng hải',
            'h\xc3\xa0ng h\xe1\xba\xa3i':'hàng hải',
            'ng\xc3\xb4n t\xe1\xbb\xab h\xc3\xa0ng h\xe1\xba\xa3i':'hàng hải',
            'th\xc6\xb0\xc6\xa1ng nghi\xe1\xbb\x87p h\xc3\xa0ng h\xe1\xba\xa3i':'hàng hải',
            'h\xe1\xba\xa3i qu\xc3\xa2n':'hàng hải',
            'ng\xc3\xb4n t\xe1\xbb\xab thu\xe1\xbb\xb7 th\xe1\xbb\xa7':'hàng hải',
            't\xc3\xa0u th\xe1\xbb\xa7y':'hàng hải',
            'ha\xcc\x80ng kh\xc3\xb4ng':'hàng không',
            'h\xc3\xa0ng kh\xc3\xb4ng':'hàng không',
            '<h\xc3\xa0ng kh\xc3\xb4ng':'hàng không',
            'ng\xc3\xb4n t\xe1\xbb\xab gi\xe1\xbb\x9bi phi c\xc3\xb4ng':'hàng không',
            'h\xc3\xacnh h\xe1\xbb\x8dc':'toán học',
            'toa\xcc\x81n ho\xcc\xa3c':'toán học',
            'to\xc3\xa1n ho\xcc\xa3c':'toán học',
            'to\xc3\xa1n h\xe1\xbb\x8dc':'toán học',
            'to\xc3\xa1n':'toán học',
            'ho\xc3\xa1 h\xe1\xbb\x8dc':'hóa học',
            'ho\xcc\x81a ho\xcc\xa3c':'hóa học',
            'h\xc3\xb3a h\xe1\xbb\x8dc':'hóa học',
            'kim lo\xe1\xba\xa1i':'hóa học',
            'h\xe1\xbb\x8fa xa':'hỏa xa',
            'h\xe1\xbb\x99i ho\xe1\xba\xa1':'hội họa',
            'h\xe1\xbb\x99i h\xe1\xbb\x8da':'hội họa',
            'h\xc3\xb4\xcc\xa3i ho\xcc\xa3a':'hội họa',
            'huy\xe1\xbb\x81n b\xc3\xad h\xe1\xbb\x8dc':'thần bí học',
            'th\xe1\xba\xa7n b\xc3\xad h\xe1\xbb\x8dc':'thần bí học',
            'kinh doanh':'kinh tế',
            'kinh t\xe1\xba\xbf h\xe1\xbb\x8dc':'kinh tế',
            'kinh t\xc3\xaa\xcc\x81':'kinh tế',
            'kinh t\xe1\xba\xbf':'kinh tế',
            'th\xe1\xbb\x8b tr\xc6\xb0\xe1\xbb\xa3ng ch\xe1\xbb\xa9ng kho\xc3\xa1n':'kinh tế',
            'ng\xc3\xa0nh ng\xc3\xa2n h\xc3\xa0ng':'kinh tế',
            'ng\xc3\xa2n h\xc3\xa0ng':'kinh tế',
            't\xc3\xadn d\xe1\xbb\xa5ng':'kinh tế',
            'qua\xcc\x89ng ca\xcc\x81o':'kinh tế',
            'ng\xc3\xb4n t\xe1\xbb\xab qu\xe1\xba\xa3ng c\xc3\xa1o':'kinh tế',
            't\xc3\xa0i ch\xc3\xadnh':'kinh tế',
            'ch\xe1\xbb\xa9ng kho\xc3\xa1n':'kinh tế',
            'n\xe1\xbb\xa3 n\xe1\xba\xa7n':'kinh tế',
            'ngo\xe1\xba\xa1i giao':'kinh tế',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xc6\xb0\xc6\xa1ng m\xe1\xba\xa1i':'kinh tế',
            'ng\xc3\xb4n t\xe1\xbb\xab thu\xc6\xa1ng m\xe1\xba\xa1i':'kinh tế',
            'ng\xc3\xb4n ng\xe1\xbb\xaf h\xc3\xa0ng ch\xc3\xadnh':'kinh tế',
            'kh\xc3\xad t\xc6\xb0\xe1\xbb\xa3ng h\xe1\xbb\x8dc':'khí tượng',
            'kh\xc3\xad t\xc6\xb0\xe1\xbb\xa3ng':'khí tượng',
            'khoa h\xe1\xba\xa3i d\xc6\xb0\xc6\xa1ng h\xe1\xbb\x8dc':'hải dương học',
            'khoa h\xe1\xbb\x8dc':'khoa học',
            'khoa h\xc3\xb9ng bi\xe1\xbb\x87n':'hùng biện',
            'khoa kha\xcc\x89o c\xc3\xb4\xcc\x89':'khoa khảo cổ',
            'khoa kh\xe1\xba\xa3o c\xe1\xbb\xa9u tri\xe1\xba\xbft h\xe1\xbb\x8dc v\xc3\xa0 kinh \xc4\x91i\xe1\xbb\x83n th\xe1\xbb\x9di c\xe1\xbb\x95':'triết học và kinh điển',
            'tri\xe1\xba\xbft ho\xcc\xa3c':'triết học và kinh điển',
            'tri\xe1\xba\xbft h\xe1\xbb\x8dc':'triết học và kinh điển',
            'khoa ng\xc3\xb4n ng\xe1\xbb\xaf':'ngôn ngữ học',
            'ng\xc3\xb4n ng\xc6\xb0\xcc\x83 ho\xcc\xa3c':'ngôn ngữ học',
            'ng\xc3\xb4n ng\xe1\xbb\xaf h\xe1\xbb\x8dc':'ngôn ngữ học',
            'ng\xc3\xb4n ng\xe1\xbb\xaf':'ngôn ngữ học',
            'bi\xe1\xba\xbfn ng\xe1\xbb\xaf h\xe1\xbb\x8dc':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab \xc4\x90\xc3\xb4ng \xc4\x90\xe1\xbb\xa9c':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab \xc4\x90\xe1\xbb\xa9c Qu\xe1\xbb\x91c x\xc3\xa3':'ngôn ngữ học',
            'khoa ph\xc3\xa2n t\xc3\xa2m h\xe1\xbb\x8dc':'phân tâm học',
            'ph\xc3\xa2n t\xc3\xa2m h\xe1\xbb\x8dc':'phân tâm học',
            'khoa si\xc3\xaau t\xc3\xa2m linh h\xe1\xbb\x8dc':'siêu tâm linh học',
            'khoa \xe1\xbb\xa9ng d\xe1\xbb\xa5ng tin h\xe1\xbb\x8dc':'ứng dụng tin học',
            'ki\xe1\xba\xbfn tr\xc3\xbac':'kiến trúc',
            'ki\xe1\xba\xbfn tr\xe1\xbb\xa5c':'kiến trúc',
            'ng\xc3\xa0nh ki\xe1\xba\xbfn tr\xc3\xbac':'kiến trúc',
            'ng\xc3\xa0nh x\xc3\xa2y c\xe1\xba\xa5t':'kiến trúc',
            'ng\xc3\xa0nh x\xc3\xa2y d\xe1\xbb\xb1ng':'kiến trúc',
            'x\xc3\xa2y d\xc6\xb0\xcc\xa3ng':'kiến trúc',
            'x\xc3\xa2y d\xe1\xbb\xb1ng':'kiến trúc',
            'kh\xc3\xb4ng gian':'kiến trúc',
            'k\xe1\xbb\xb9 thu\xc3\xa2t':'kỹ thuật',
            'k\xe1\xbb\xb9 thu\xe1\xba\xadt':'kỹ thuật',
            'k\xe1\xbb\xb9 thu\xe1\xba\xadt-':'kỹ thuật',
            'ky\xcc\x83 thu\xc3\xa2\xcc\xa3t':'kỹ thuật',
            'l\xe1\xbb\x8bch s\xe1\xbb\xad':'lịch sử',
            'lu\xc3\xa2n l\xc3\xbd h\xe1\xbb\x8dc':'luân lý học',
            'l\xc3\xbd lu\xe1\xba\xadn h\xe1\xbb\x8dc':'lý luận học',
            'lu\xc3\xa2\xcc\xa3t ho\xcc\xa3c':'luật',
            'lu\xe1\xba\xadt':'luật',
            'lu\xe1\xba\xadt ho\xcc\xa3c':'luật',
            'lu\xe1\xba\xadt h\xe1\xbb\x8dc':'luật',
            'lu\xe1\xba\xadt ph\xc3\xa1p':'luật',
            'ng\xc3\xb4n t\xe1\xbb\xab lu\xe1\xba\xadt h\xe1\xbb\x8dc':'luật',
            'ng\xc3\xb4n ng\xe1\xbb\xaf lu\xc3\xa2t ph\xc3\xa1p':'luật',
            'ng\xc3\xb4n ng\xe1\xbb\xaf lu\xe1\xba\xadt ph\xc3\xa1p':'luật',
            'c\xc3\xb4ng quy\xe1\xbb\x81n':'luật',
            'ng\xc3\xb4n ng\xe1\xbb\xaf c\xc3\xb4ng quy\xe1\xbb\x81n':'luật',
            '\xc3\xa1n ph\xe1\xba\xa1t':'luật',
            'ng\xc3\xb4n t\xe1\xbb\xab ph\xc3\xa1p lu\xe1\xba\xadt':'luật',
            'luy\xe1\xbb\x87n kim':'luyện kim',
            'ng\xc3\xa0nh th\xe1\xbb\xa3 ngu\xe1\xbb\x99i':'luyện kim',
            'ng\xc3\xa0nh ch\xe1\xba\xbf t\xe1\xba\xa1o phi c\xc6\xa1':'chế tạo phi cơ',
            'm\xc3\xa1y bay':'chế tạo phi cơ',
            'nga\xcc\x80nh d\xc3\xaa\xcc\xa3t':'ngành dệt',
            'ng\xc3\xa0nh d\xe1\xbb\x87t':'ngành dệt',
            'ng\xc3\xa0nh du l\xe1\xbb\x8bch':'du lịch',
            'ng\xc3\xa0nh in':'ngành in',
            'ng\xc3\xb4n t\xe1\xbb\xab ngh\xe1\xbb\x81 in s\xc3\xa1ch':'ngành in',
            '\xc3\xa2n lo\xc3\xa1t':'ngành in',
            '\xe1\xba\xa5n lo\xc3\xa1t':'ngành in',
            'ng\xc3\xa0nh kh\xc3\xb4ng gian':'vũ trụ học',
            'v\xc5\xa9 tr\xe1\xbb\xa5 h\xe1\xbb\x8dc':'vũ trụ học',
            'ng\xc3\xa0nh m\xe1\xbb\x99c':'ngành mộc',
            'ng\xc3\xa0nh ngh\xe1\xbb\x87 thu\xe1\xba\xadt':'nghệ thuật',
            'ngh\xe1\xbb\x87 thu\xe1\xba\xadt t\xc6\xb0\xe1\xbb\xa3ng h\xc3\xacnh':'nghệ thuật',
            'ngh\xe1\xbb\x87 thu\xe1\xba\xadt':'nghệ thuật',
            'ng\xc3\xa0nh th\xc6\xb0 vi\xe1\xbb\x87n':'ngành thư viện',
            'ng\xc3\xa0nh th\xc3\xba y':'ngành thú y',
            'y khoa th\xc3\xba y':'ngành thú y',
            'nhi\xc3\xaa\xcc\x81p a\xcc\x89nh':'nhiếp ảnh',
            'nhi\xe1\xba\xbfp \xe1\xba\xa3nh':'nhiếp ảnh',
            'ng\xe1\xbb\xaf v\xe1\xbb\xb1ng h\xe1\xbb\x8dc':'văn học',
            't\xe1\xbb\xa5c ng\xe1\xbb\xaf':'văn học',
            'tu\xcc\xa3c ng\xc6\xb0\xcc\x83':'văn học',
            'v\xc4\x83n ch\xc6\xb0\xc6\xa1ng':'văn học',
            'v\xc4\x83n h\xe1\xbb\x8dc':'văn học',
            'v\xc4\x83n ph\xe1\xba\xa1m':'văn học',
            'v\xc4\x83n th\xc6\xa1':'văn học',
            'th\xc6\xa1 v\xc4\x83n':'văn học',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xc6\xa1 v\xc4\x83n':'văn học',
            'ng\xc3\xb4n t\xe1\xbb\xab v\xc4\x83n t\xe1\xbb\xb1':'văn học',
            'ng\xc3\xb4n t\xe1\xbb\xb1 v\xc4\x83n t\xe1\xbb\xb1':'văn học',
            'qu\xc3\xa2n \xc4\x91\xe1\xbb\x99i':'quân sự',
            'ng\xc3\xb4n t\xe1\xbb\xab qu\xc3\xa2n \xc4\x91\xe1\xbb\x99i':'quân sự',
            'qu\xc3\xa2n s\xc6\xb0\xcc\xa3':'quân sự',
            'qu\xc3\xa2n s\xe1\xbb\xb1':'quân sự',
            'qu\xc3\xa2n s\xe1\xbb\xb1-':'quân sự',
            'ng\xc3\xb4n t\xe1\xbb\xab binh l\xc3\xadnh':'quân sự',
            'ng\xc3\xb4n t\xe1\xbb\xab binh s\xc4\xa9':'quân sự',
            'sinh l\xc3\xbd h\xe1\xbb\x8dc':'sinh lý',
            'sinh l\xc3\xbd':'sinh lý',
            't\xc3\xa2m ly\xcc\x81 ho\xcc\xa3c':'tâm lý học',
            't\xc3\xa2m ly\xcc\x81 h\xe1\xbb\x8dc':'tâm lý học',
            't\xc3\xa2m l\xc3\xad h\xe1\xbb\x8dc':'tâm lý học',
            't\xc3\xa2m l\xc3\xbd':'tâm lý học',
            't\xc3\xa2m l\xc3\xbd h\xe1\xbb\x8dc':'tâm lý học',
            't\xc3\xa2m sinh l\xc3\xbd h\xe1\xbb\x8dc':'tâm lý học',
            'm\xc3\xb4n b\xe1\xbb\x87nh t\xc3\xa2m l\xc3\xbd h\xe1\xbb\x8dc':'tâm lý học',
            'th\xe1\xba\xa7n h\xe1\xbb\x8dc':'thần học',
            'th\xe1\xba\xa7n tho\xe1\xba\xa1i':'thần thoại',
            'th\xe1\xba\xa7n tho\xe1\xba\xa1i Hy L\xe1\xba\xa1p':'thần thoại',
            'th\xe1\xba\xa7n tho\xe1\xba\xa1i \xc4\x90\xe1\xbb\xa9c':'thần thoại',
            'th\xe1\xba\xa7n th\xe1\xbb\x8dai':'thần thoại',
            'thi\xc3\xaan v\xc4\x83n':'thiên văn học',
            'thi\xc3\xaan v\xc4\x83n hoc':'thiên văn học',
            'thi\xc3\xaan v\xc4\x83n ho\xcc\xa3c':'thiên văn học',
            'thi\xc3\xaan v\xc4\x83n h\xe1\xbb\x8dc':'thiên văn học',
            'th\xe1\xbb\x91ng k\xc3\xaa h\xe1\xbb\x8dc':'thống kê học',
            'th\xe1\xbb\x91ng k\xc3\xaa':'thống kê học',
            'tr\xe1\xba\xafc \xc4\x91\xe1\xbb\x8ba h\xe1\xbb\x8dc':'trắc địa học',
            'truy\xe1\xbb\x81n h\xc3\xacnh':'truyền hình',
            'truy\xe1\xbb\x81n thanh':'truyền hình',
            'x\xc3\xa3 h\xe1\xbb\x99i h\xe1\xbb\x8dc':'xã hội học',
            'x\xc3\xa3 h\xe1\xbb\x99i':'xã hội học',
            '\xc4\x91\xe1\xbb\x93 g\xe1\xbb\x91m':'gốm',
            'ch\xc4\x83n nu\xc3\xb4i th\xc3\xba v\xe1\xba\xadt':'chăn nuôi',
            'ng\xc3\xb4n ng\xe1\xbb\xaf ng\xc6\xb0\xe1\xbb\x9di nu\xc3\xb4i ong':'chăn nuôi',
            'gia s\xc3\xbac':'chăn nuôi',
            '\xc4\x91\xc3\xb4\xcc\xa3ng v\xc3\xa2t ho\xcc\xa3c':'động vật học',
            '\xc4\x91\xe1\xbb\x99ng v\xe1\xba\xadt':'động vật học',
            '\xc4\x91\xe1\xbb\x99ng v\xe1\xba\xadt h\xe1\xbb\x8dc':'động vật học',
            'lo\xc3\xa0i \xc4\x91\xe1\xbb\x99ng v\xe1\xba\xadt r\xe1\xbb\x97ng':'động vật học',
            'c\xc3\xa1 m\xe1\xba\xadp':'động vật học',
            'chim ch\xc3\xb3c':'động vật học',
            'k\xe1\xba\xbf to\xc3\xa1n':'kế toán',
            'th\xe1\xbb\xa9c v\xe1\xba\xadt h\xe1\xbb\x8dc':'thực vật học',
            'th\xe1\xbb\xb1c v\xe1\xba\xadt hoc':'thực vật học',
            'th\xe1\xbb\xb1c v\xe1\xba\xadt h\xe1\xbb\x8dc':'thực vật học',
            'th\xe1\xbb\xb1c v\xe1\xba\xadt':'thực vật học',
            'nha khoa':'nha khoa',
            's\xc4\x83n b\xe1\xba\xafn':'săn bắn',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\xa3 s\xc4\x83n':'săn bắn',
            'c\xc3\xb4ng vi\xe1\xbb\x87c':'nghề nghiệp',
            'ti\xe1\xbb\x81n t\xe1\xbb\x87':'tiền tệ',
            'ti\xe1\xbb\x81n b\xe1\xba\xa1c c\xe1\xba\xafc':'tiền tệ',
            '\xc4\x91\xc3\xa1ng ti\xe1\xbb\x81n':'tiền tệ',
            'ng\xc3\xb4n t\xe1\xbb\xab gi\xe1\xbb\x9bi tr\xe1\xba\xbb':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a d\xc3\xa2n da \xc4\x91\xe1\xbb\x8f':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n giang h\xe1\xbb\x93':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab giang h\xe1\xbb\x93':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab trong gi\xe1\xbb\x9bi giang h\xe1\xbb\x93':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a d\xc3\xa2n l\xc3\xa0ng ch\xc6\xa1i':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n ch\xc6\xa1i':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab l\xc3\xa0ng ch\xc6\xa1i':'ngôn ngữ học',
            'ng\xc3\xb4n ng\xe1\xbb\xaf h\xe1\xbb\x8dc tr\xc3\xb2':'ngôn ngữ học',
            'ng\xc3\xb4n ng\xe1\xbb\xaf thanh thi\xe1\xba\xbfu ni\xc3\xaan':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a sinh vi\xc3\xaan':'ngôn ngữ học',
            'ng\xc3\xb4n t\xe1\xbb\xab sinh vi\xc3\xaan':'ngôn ngữ học',
            'kh\xc3\xa2\xcc\x89u ng\xc6\xb0\xcc\x83':'ngôn ngữ học',
            'kh\xe1\xba\xa9u  ng\xe1\xbb\xaf':'ngôn ngữ học',
            'kh\xe1\xba\xa9u ng\xe1\xbb\xaf':'ngôn ngữ học',
            'kh\xe1\xba\xa9u ng\xe1\xbb\xaf: k\xe1\xba\xbft qu\xe1\xba\xa3':'ngôn ngữ học',
            'th\xc3\xb4 t\xe1\xbb\xa5c':'',
            'm\xe1\xbb\x89a  mai':'',
            'm\xe1\xbb\x89a mai':'',
            'mi\xe1\xbb\x87t th\xe1\xbb\x8b':'',
            'khinh mi\xe1\xbb\x87t':'',
            '- ng\xc3\xb4n t\xe1\xbb\xab tr\xe1\xba\xbb con':'',
            'ng\xc3\xb4n t\xe1\xbb\xab tr\xe1\xba\xbb con':'',
            'ng\xc3\xb4n t\xe1\xbb\xab chuy\xc3\xaan khoa':'',
            'ti\xe1\xba\xbfng l\xc3\xb3ng c\xe1\xbb\xa7a tu\xe1\xbb\x95i tr\xe1\xba\xbb':'',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n h\xc3\xbat x\xc3\xac ke':'',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a d\xc3\xa2n b\xe1\xbb\x8bp b\xe1\xbb\xa3m':'',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a gi\xe1\xbb\x9bi b\xe1\xba\xa5t l\xc6\xb0\xc6\xa1ng':'',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a gi\xe1\xbb\x9bi phi c\xc3\xb4ng':'',
            'ng\xc3\xb4n t\xe1\xbb\xab gi\xe1\xbb\x9bi b\xe1\xba\xa5t l\xc6\xb0\xc6\xa1ng':'',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n mi\xe1\xbb\x81n n\xc3\xbai':'',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n anh ch\xe1\xbb\x8b':'',
            'ng\xc3\xb4n t\xe1\xbb\xab g\xe1\xbb\x8di th\xc3\xa2n m\xe1\xba\xadt':'',
            'g\xe1\xbb\x8di t\xe1\xba\xaft':'viết tắt',
            'g\xe1\xbb\x8di t\xe1\xba\xaft c\xe1\xbb\xa7a':'viết tắt',
            'th\xc3\xb4 l\xe1\xbb\x97':'',
            'ng\xc3\xb4n t\xe1\xbb\xab b\xc3\xacnh d\xc3\xa2n':'',
            'di\xe1\xbb\x83u c\xe1\xbb\xa3t':'',
            'di\xe1\xbb\x85u c\xe1\xbb\xa3t':'',
            'gii\xe1\xbb\x85u c\xe1\xbb\xa3t':'',
            'gi\xe1\xbb\x85u c\xe1\xbb\xa3t':'',
            '<gi\xe1\xbb\x85u c\xe1\xbb\xa3t':'',
            'ng\xc3\xb4n ng\xe1\xbb\xaf b\xc3\xacnh d\xc3\xa2n':'',
            'nh\xe1\xbb\x8bp tim':'',
            
            'h\xc3\xa0ng ho\xc3\xa1':'kinh tế',
            'h\xc3\xa0ng th\xe1\xbb\xa7y tinh':'kinh tế',
            
            '-':'',
            '- ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n ma t\xc3\xbay':'',
            '- nh\xe1\xba\xa3y ngang b\xc3\xaan s\xc6\xb0\xe1\xbb\x9dn qua ch\xc6\xb0\xe1\xbb\x9bng ng\xe1\xba\xa1i v\xe1\xba\xadt':'',
            '--':'',
            '-t\xe1\xbb\xab m\xc6\xb0\xe1\xbb\xa3n':'từ mượn',
            '1933- 1945':'',
            '2.':'',
            '2. [s\xe1\xbb\xb1]  gia c\xe1\xbb\x91':'',
            '2. c\xc3\xa2y trinh n\xe1\xbb\xaf th\xc6\xb0\xe1\xbb\x9dng':'',
            '2. thu x\xe1\xba\xbfp':'',
            '2. th\xc3\xb4ng b\xc3\xa1o':'',
            'B\xc3':'',
            'B\xc3\xa0':'',
            'Cornusmas L':'',
            'D':'',
            'Etymologie':'',
            'Ph\xe1\xba\xa3n ngh\xc4\xa9a':'phản nghĩa',
            'Semantik':'',
            '[kh\xe1\xbb\x8fan m\xc3\xb3n] \xc6\xb0u \xc4\x91\xc3\xa3i':'',
            'bi\xe1\xba\xbfn \xc4\x91\xe1\xbb\x95i':'',
            'b\xc3\xa0n':'',
            'b\xc3\xa1nh xe':'',
            'b\xc3\xacnh qu\xc3\xa2n ho\xc3\xa1':'',
            'b\xc6\xa1':'',
            'b\xe1\xba\xb1ng ch\xe1\xbb\xa9ng \xe2\x80\xa6':'',
            'b\xe1\xbb\x87nh':'',
            'chi\xe1\xba\xbfn \xc4\x91\xe1\xba\xa5u':'',
            'chi\xe1\xba\xbfu c\xe1\xbb\x91':'',
            'chuy\xe1\xbb\x83n':'',
            'ch\xc6\xb0\xc6\xa1ng tr\xc3\xacnh':'',
            'ch\xe1\xba\xa5t li\xe1\xbb\x87u':'',
            'ch\xe1\xba\xb7t':'',
            'ch\xe1\xbb\xa9c v\xe1\xbb\xa5':'',
            'ch\xe1\xbb\xafa l\xe1\xbb\xada':'',
            'c\xc3\xa0o b\xe1\xba\xb1ng':'',
            'c\xc3\xa1i g\xc3\xac':'',
            'c\xc6\xa1 quan':'',
            'c\xe1\xbb\xa7a \xc4\x91\xc4\xa9a b\xe1\xba\xafn':'',
            'c\xe1\xbb\xa7ng c\xe1\xbb\x91':'',
            'd\xc3\xa2y':'',
            'd\xc3\xadnh \xe2\x80\xa6 v\xc3\xa0o':'',
            'ghim':'',
            'gh\xc3\xa9p':'',
            'gi\xe1\xba\xa3i quy\xe1\xba\xbft tho\xe1\xba\xa3 \xc4\x91\xc3\xa1ng':'',
            'gi\xe1\xba\xa3i th\xc3\xadch':'',
            'gi\xe1\xba\xa3ng gi\xe1\xba\xa3i':'',
            'gi\xe1\xbb\xaf':'',
            'g\xc3\xa0i':'',
            'g\xc3\xa2y ra':'',
            'g\xc4\x83m':'',
            'g\xe1\xbb\x93m: Lexikologie':'',
            'g\xe1\xbb\xa3i ra':'',
            'h':'',
            'hu\xe1\xba\xa5n luy\xe1\xbb\x87n':'',
            'h\xc3\xa0ng':'',
            'h\xc6\xb0\xe1\xbb\x9bng \xc4\x91i':'',
            'h\xe1\xbb\x89 \xc4\x91\xe1\xbb\x93ng':'',
            'h\xe1\xbb\x8dc sinh':'',
            'khi\xe1\xba\xbfn cho':'',
            'khk\xc3\xb4ng thay \xc4\x91\xe1\xbb\x95i':'',
            'kho\xe1\xba\xbb m\xe1\xba\xa1nh':'',
            'khuy\xc3\xaan d\xe1\xbb\x97':'',
            'khuy\xc3\xaan nh\xe1\xbb\xa7':'',
            'kh\xc3\xaau g\xe1\xbb\xa3i':'',
            'kh\xc3\xaau l\xc3\xaan':'',
            'kh\xc3\xb4ng':'',
            'kh\xc3\xb4ng ti\xe1\xba\xbfn l\xc3\xaan \xc4\x91\xc6\xb0\xe1\xbb\xa3c':'',
            'ki\xc3\xaau c\xc4\x83ng':'',
            'ki\xe1\xbb\x87n to\xc3\xa0n':'',
            'k\xc3\xa9o':'',
            'k\xc3\xad hi\xe1\xbb\x87u':'kí hiệu',
            'k\xc3\xbd hi\xe1\xbb\x87u':'kí hiệu',
            'k\xe1\xba\xb9p':'',
            'lo\xe1\xba\xa1i nh\xe1\xbb\x8f tu\xe1\xbb\x95i':'',
            'l\xc3\xa0m cho':'',
            'l\xc3\xa0m cho hi\xe1\xbb\x83u':'',
            'l\xc3\xa0m ngang b\xe1\xba\xb1ng':'',
            'l\xc3\xa0m \xe2\x80\xa6 m\xc3\xaa m\xe1\xba\xa9n':'',
            'm':'',
            'mu\xe1\xbb\x91n \xc4\x91\xe1\xbb\x95':'',
            'm\xe1\xba\xafc n\xe1\xbb\xa3':'',
            'ngang nhau':'',
            'nghi\xcc\x83a bo\xcc\x81ng':'nghĩa bóng',
            'ngh\xc4\xa9 t\xc3\xb4i l\xc3\xa0 h\xe1\xba\xa1ng ng\xc6\xb0\xe1\xbb\x9di th\xe1\xba\xbf n\xc3\xa0o?':'',
            'ngh\xc4\xa9a':'',
            'ngh\xc4\xa9a b\xc3\xb3ng':'nghĩa bóng',
            'ng\xc3\xb4n ng\xe1\xbb\xaf d\xc3\xa2n x\xc3\xac ke':'ngôn ngữ nói',
            'ng\xc3\xb4n ng\xe1\xbb\xaf thu\xe1\xbb\xb7 th\xe1\xbb\xa7':'ngôn ngữ nói',
            'ng\xc3\xb4n ng\xe1\xbb\xaf th\xe1\xbb\xa3 s\xc4\x83n':'ngôn ngữ nói',
            'ng\xc3\xb4n ng\xe1\xbb\xaf tr\xe1\xba\xbb con':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab chuy\xc3\xaan m\xc3\xb4n':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a h\xe1\xbb\x8dc sinh':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a l\xc3\xadnh':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a th\xe1\xbb\xa3 s\xc4\x83n':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab c\xe1\xbb\xa7a tr\xe1\xba\xbb con':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n gian':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n ma tu\xc3\xbd':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n ma t\xc3\xbay':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n phi x\xc3\xac ke':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab d\xc3\xa2n x\xc3\xac ke':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab h\xc3\xa0nh ch\xc3\xadnh':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab h\xe1\xbb\x8dc sinh':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab h\xe1\xbb\x8dc tr\xc3\xb2':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab lu\xe1\xba\xadt ph\xc3\xa1p':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab thanh ni\xc3\xaan':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab thanh thi\xe1\xba\xbfu ni\xc3\xaan':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab thanh thi\xe1\xba\xbfu ni\xc3\xaan>':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\x9di ph\xc3\xa1t x\xc3\xadt \xc4\x90\xe1\xbb\xa9c':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\x9di \xc4\x90\xe1\xbb\xa9c qu\xe1\xbb\x91c x\xc3\xa3':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\xa3 m\xe1\xbb\x8f':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\xa7y th\xe1\xbb\xa7':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab th\xe1\xbb\xa7y th\xe1\xbb\xa7y':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab x\xc3\xac ke':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xab \xc3\xa1m ch\xe1\xbb\x89 chung c\xe1\xbb\xa7a th\xe1\xbb\xa3 s\xc4\x83n':'ngôn ngữ nói',
            'ng\xc3\xb4n t\xe1\xbb\xb1 th\xe1\xbb\xa3 s\xc4\x83n':'ngôn ngữ nói',
            'ng\xc6\xb0\xe1\xbb\x9di':'người',
            'ng\xc6\xb0\xe1\xbb\x9di n\xc3\xa0o \xc4\x91\xe1\xbb\xa9ng d\xe1\xba\xady':'',
            'ng\xe1\xbb\xaf nguy\xc3\xaan h\xe1\xbb\x8dc':'',
            'nh\xc3\xa0 m\xc3\xa1y':'',
            'nh\xc6\xb0 ch\xe1\xbb\xaf b':'',
            'ph\xc3\xa1i  n\xe1\xbb\xaf':'phái nữ',
            'ph\xc3\xa1i nam':'phái nam',
            'ph\xc3\xa1i n\xe1\xbb\xaf':'phái nữ',
            'ph\xe1\xbb\xa5 n\xe1\xbb\xaf':'phái nữ',
            'ph\xc3\xa9p l\xe1\xbb\x8bch s\xe1\xbb\xb1':'',
            'ph\xe1\xba\xa3n ngh\xc4\xa9a':'phản nghĩa',
            'ph\xe1\xba\xa3n \xc4\x91\xe1\xbb\x91i c\xc3\xa1i g\xc3\xac \xe2\x80\xa6.':'',
            
            'quan \xc4\x91i\xe1\xbb\x83m':'',
            'quang h\xe1\xbb\x8dc':'',
            'quy\xe1\xbb\x81n \xc6\xb0u ti\xc3\xaan':'',
            'r\xc6\xb0\xe1\xbb\xa3u':'',
            's':'',
            'sai':'',
            'san b\xe1\xba\xb1ng':'',
            'san ph\xe1\xba\xb3ng':'',
            'say \xc4\x91\xe1\xba\xafm':'',
            'sinh vi\xc3\xaan':'',
            'si\xe1\xba\xbft':'',
            's\xc3\xa1p':'',
            's\xe1\xba\xafc b\xc3\xa9n':'',
            's\xe1\xbb\xa9c l\xe1\xbb\xb1c':'',
            's\xe1\xbb\xb1 h\xc4\x83ng say':'',
            'theo th\xe1\xbb\xa9 t\xe1\xbb\xb1':'',
            'thuy\xe1\xba\xbft ph\xe1\xbb\xa5c':'',
            'thu\xe1\xbb\x91c m\xc3\xaa':'',
            'th\xe1\xba\xb1ng \xe1\xbb\x9f':'',
            'th\xe1\xbb\x95 ng\xe1\xbb\xaf':'ngôn ngữ học',
            'th\xe1\xbb\xb1c ph\xe1\xba\xa9m':'kinh tế',
            'ti\xe1\xba\xbfng La tinh':'ngôn ngữ học',
            'ti\xe1\xba\xbfng g\xe1\xbb\x8di':'ngôn ngữ học',
            'ti\xe1\xba\xbfp n\xe1\xbb\x91i':'liên từ',
            'ti\xe1\xbb\x83u \xc4\x91\xe1\xbb\x93ng':'',
            'trong m\xe1\xbb\x99t v\xe1\xbb\x8b tr\xc3\xad kh\xc3\xb4ng gi\xe1\xbb\xaf \xc4\x91\xc6\xb0\xe1\xbb\xa3c':'',
            't\xc3\xb4i th\xc3\xadch l\xc3\xa0m \xc4\x91i\xe1\xbb\x81u \xe1\xba\xa5y !':'',
            't\xe1\xba\xa5t c\xe1\xba\xa3 nh\xe1\xbb\xafng g\xc3\xac h\xe1\xba\xafn c\xc3\xb3':'',
            't\xe1\xba\xb7ng':'',
            't\xe1\xbb\x95ng  qu\xc3\xa1t':'',
            't\xe1\xbb\x95ng qu\xc3\xa1t':'',
            't\xe1\xbb\x97ng qu\xc3\xa1t':'',
            't\xe1\xbb\xab chuy\xc3\xaan m\xc3\xb4n':'',
            't\xe1\xbb\xab m\xc6\xb0\xe1\xbb\xa3n':'từ mượn',
            't\xe1\xbb\xab m\xc6\xb0\xe1\xbb\xa3n-':'từ mượn',
            'vi\xe1\xba\xbft t\xe1\xba\xaft':'viết tắt',
            'vi\xe1\xba\xbft t\xe1\xba\xaft c\xe1\xbb\xa7a':'viết tắt',
            'v\xc3\xa0o':'',
            'v\xc3\xa1n':'',
            'v\xc3\xad d\xe1\xbb\xa5 \xe1\xbb\x9f ph\xc3\xb3 m\xc3\xa1t 40% Fett in der Trockensubstanz :':'',
            'xe h\xc6\xa1i':'',
            'x\xe1\xba\xa5u':'',
            'x\xe1\xba\xbfp \xc4\x91\xe1\xba\xb7t':'',
            '\xc3\xa1p gi\xe1\xba\xa3i':'',
            '\xc3\xbd th\xc3\xadch':'',
            '\xc4\x91i\xe1\xbb\x87u':'',
            '\xc4\x91ua ng\xe1\xbb\xb1a':'',
            '\xc4\x91\xc3\xa0n b\xc3':'',
            '\xc4\x91\xc3\xa0n b\xc3\xa0':'',
            '\xc4\x91\xc3\xa0n b':'',
            '\xc4\x91\xc3\xadnh':'',
            '\xc4\x91\xe1\xba\xb7c quy\xe1\xbb\x81n':'',
            '\xc4\x91\xe1\xbb\x83 s\xe1\xbb\xad d\xe1\xbb\xa5ng':'',
            '\xc4\x91\xe1\xbb\x83 \xc4\x91\xc3\xa1nh l\xe1\xba\xa1c h\xc6\xb0\xe1\xbb\x9bng \xc4\x91\xe1\xbb\x8bch qu\xc3\xa2n':'',
            '\xc4\x91\xe1\xbb\x83 \xe2\x80\xa6v\xc3\xa0o':'',
            '\xc4\x91\xe1\xbb\x93ng  ngh\xc4\xa9a':'đồng nghĩa',
            '\xc4\x91\xe1\xbb\x93ng ngh\xc4\xa9a':'đồng nghĩa',
            '\xc4\x91\xe1\xbb\xa1':'',
            '\xe1\xbb\x95n th\xe1\xbb\x8fa':'',
            '\xe1\xbb\xa7ng h\xe1\xbb\x99 c\xc3\xa1i g\xc3\xac':'',
            'j\xc3\xbcngere Schwester zu \xc3\xa4lteren Geschwistern':'',
            '\xc3\xa4ltere Geschwister zu j\xc3\xbcngeren Geschwistern':'',
            'junges M\xc3\xa4dchen zum Geliebten':'',
            'junger Mann zur Geliebten':'',
            'Ehefrau zum Ehemann':'',
            'Ehemann zur Ehefrau':'',
            'phụ nữ': '',
            'ng\xc3\xb4n ng\xe1\xbb\xaf h\xc3\xa0nh ch\xc3\xadnh':'',
            'ti\xe1\xba\xbfng Ph\xc3\xa1p':'ngôn ngữ nói',
            'gi\xe1\xba\xa3i ph\xe1\xba\xa9u':'giải phẫu học',
            'ti\xe1\xba\xbfng Anh':'ngôn ngữ nói',
            'c\xc5\xa9ng c\xc3\xb3 ngh\xc4\xa9a r\xe1\xbb\x99ng':'nghĩa rộng',
            'ti\xe1\xba\xbfng Nga':'',
            '\xc4\x91\xe1\xba\xa1o Cao \xc4\x90\xc3\xa0i':'tôn giáo',
            'ti\xe1\xba\xbfng t\xe1\xbb\xa5c':'ngôn ngữ nói',
            'ch\xe1\xbb\xaf c\xc3\xa2m':'ngôn ngữ nói',
            'ti\xe1\xba\xbfng ch\xe1\xbb\xadi':'ngôn ngữ nói',
            'chuy\xe1\xbb\x87n c\xe1\xbb\x95 t\xc3\xadch':'văn học',
            'th\xc6\xa1 b\xc3\xa1t c\xc3\xba':'văn học',
            'v\xc4\x83n ngh\xe1\xbb\x87':'văn học',
            't\xc3\xadn ng\xc6\xb0\xe1\xbb\xa1ng':'tôn giáo',
            'Ph\xc3\xa2t gi\xc3\xa1o H\xc3\xb2a h\xe1\xba\xa3o':'tôn giáo',
            'th\xc6\xa1':'thơ văn',
            'phong \xc4\x91\xc3\xb2n g\xc3\xa1nh':'y học',
            'rau sam':'thực vật học',
            'c\xc3\xa2y sung':'thực vật học',
            'ch\xc3\xadnh tri':'chính trị',
            'nh\xc4\xa9 h\xe1\xbb\x8dc':'y học',
            'con c\xc3\xb3c':'động vật học',
            'axit trong d\xe1\xba\xa1 d\xc3\xa0y':'hóa học',
            'kho\xc3\xa1ng ch\xe1\xba\xa5t h\xe1\xbb\x8dc':'kho\xc3\xa1ng ch\xe1\xba\xa5t h\xe1\xbb\x8dc',
            'con r\xe1\xba\xafn':'động vật học',
            't\xe1\xbb\xa5c':'ngôn ngữ nói',
            '\xc4\x90\xe1\xba\xa1o gi\xc3\xa1o':'tôn giáo',
            'ch\xe1\xbb\xadi t\xe1\xbb\xa5c':'ngôn ngữ nói',
            'd\xc6\xb0\xe1\xbb\xa3c':'dược học'
        }
                   
        branche = Branche.objects.get(id=brancheId)
        if not branche:
            branche = Branche(id=brancheId, text="branche%s" % brancheId)
            branche.save()
            
        branches = res
        lbrid = []
        br1 = []

class ExcelParser(BaseParser):
    def parse(self, file_name="/var/www/BachViet/data/File-Excels/A_v4_3.xls"):
        for sheetName, values in parse_xls(file_name, 'utf8'):
            for rowIdx, colIdx in sorted(values.keys()):
                v = values[(rowIdx, colIdx)]
                print 'haha ', v.encode('utf8')
                if isinstance(v, unicode):
                    v = v#.encode('utf8', 'backslashreplace')
                else:
                    v = str(v)
                v = v.strip()
                
                self.__process(rowIdx, colIdx, v)
                
    def __process(self, row_idx, col_idx, value):
        
        if col_idx == 0 and len(value) > 0:
            lst_words = Word.objects.filter(text=value, lang=self.lang)
            if len(lst_words):
                self.word = lst_words[0]
                #self.writeLog("Word: %s - Update" % value)
            else:
                self.word = Word(text=value, lang=self.lang)
                self.word.save()
                #self.writeLog("Word: %s - Add new" % value)
                self.writeWordHistory(self.word.id)
        else:
            if (col_idx == 2) and len(value) > 0:
                res = self.getBranches(value)
                arr_txt_branche = []
                
                if (res['branche1']): 
                    txt_br1 = ','.join(res['branche1'])
                    if txt_br1 and res['branche1'][0] not in self.arr_br1: 
                        arr_txt_branche.append(txt_br1)
                    
                if (res['branche2']): 
                    br2 = res['branche2'].get('br2', [])
                    txt_br2 = ','.join(br2)
                    if txt_br2 and br2[0] not in self.arr_br1: 
                        arr_txt_branche.append(txt_br2)
                    
                if (res['branche3']): 
                    txt_br3 = ','.join(res['branche3'])
                    if txt_br3 and res['branche3'][0] not in self.arr_br3: 
                        arr_txt_branche.append(txt_br3)
                    
                txt_branche = ''
                if arr_txt_branche: 
                    txt_branche =' (' + ','.join(arr_txt_branche) + ')'
                
                if (self.lang == 0):
                    if(re.search('\([a-z]\)',res['str'])):
                        p = re.compile('(\([a-z]\))', re.VERBOSE)
                        res['str'] = p.sub(r'(noun)\1',res['str'])
                
                word_translate = res['str'].strip() + txt_branche
                
                word_translates = WordTranslate.objects.filter(text=word_translate, wordId=self.word.id)
                if len(word_translates):
                    self.word_translate = word_translates[0]
                else:
                    #self.writeLog("New translate: %s" % word_translate)
                    self.word_translate = WordTranslate(text=word_translate, wordId=self.word.id)
                    self.word_translate.save()
                    self.writeWordHistory(self.word.id, status=1, update_type='add_word_translate', update_description=self.word_translate.id)
                
                """
                if (res['branche1']):
                    self.portToTablesType(res['branche1'], 1, WordTranslateType, wordtranslate_id)
                    print "res branche1: %s" % res['branche1']
                    
                if (res['branche2']):
                    self.portToTablesType()
                    print "res branche2: %s" % res['branche2']
                    
                if (res['branche3']):
                    self.portToTablesType()
                    print "res branche3: %s" % res['branche3']
                    """
            else:
                if (col_idx == 3) and (len(value) > 0):
                    if self.word_translate is None:
                        self.word_translate = WordTranslate(text= '', wordId=self.word.id)
                        self.word_translate.save()
                        
                    examples = Example.objects.filter(text=value, wordTranslateId=self.word_translate.id)
                    if len(examples):
                        self.example = examples[0]
                    else:
                        #self.writeLog("New example: %s" % value)
                        self.example = Example(text=value, wordTranslateId=self.word_translate.id)
                        self.example.save()
                        self.writeWordHistory(self.word.id, status=1, update_type='add_example', update_description=self.example.id)
                else:
                    if (col_idx == 5) and (len(value) > 0):
                        res = self.getBranches(value)
                        arr_txt_branche = []
                        if (res['branche1']): 
                            txt_br1 = ','.join(res['branche1'])
                            if txt_br1 and res['branche1'][0] not in self.arr_br1: 
                                arr_txt_branche.append(txt_br1)
                                
                        if (res['branche2']): 
                            br2 = res['branche2'].get('br2', [])
                            txt_br2 = ','.join(br2)
                            if txt_br2 and br2[0] not in self.arr_br1: 
                                arr_txt_branche.append(txt_br2)
                                
                        if (res['branche3']): 
                            txt_br3 = ','.join(res['branche3'])
                            if txt_br3 and res['branche3'][0] not in self.arr_br3: 
                                arr_txt_branche.append(txt_br3)
                                
                        txt_branche = ''
                        if arr_txt_branche: 
                            txt_branche =' (' + ','.join(arr_txt_branche) + ')'
                            
                        example_translates = ExampleTranslate.objects.filter(text=res['str'].strip()+txt_branche, exampleId=self.example.id)
                        if len(example_translates):
                            self.example_translate = example_translates[0]
                        else:
                            #self.writeLog("New example translate: %s" % res['str'].strip()+txt_branche)
                            self.example_translate = ExampleTranslate(text=res['str'].strip()+txt_branche, exampleId=self.example.id)
                            self.example_translate.save()
                            self.writeWordHistory(self.word.id, status=1, update_type='add_example_translate', update_description=self.example_translate.id)

class HtmlParser(BaseParser):
    pass

class BaseTest():
    def foo(self):
        self.__foo()
    
    def __foo(self):
        print "origin foo"
        
class ExTest(BaseTest):
    def foo2(self):
        print "extend foo"

from bachviet.apps.tt_dictionary.models import SearchHistory
import datetime
from django.db import models, connection
from bachviet.libs.utils import dictFetchAll, StringWithTitle
import re, MySQLdb

def getAnalytics():
    try:
        cursor = connection.cursor()
        query = "SELECT count(id) as numberlookup FROM SearchHistory WHERE CURRENT_DATE() = DATE(tstamp)"
        cursor.execute(query)
        result = dictFetchAll(cursor)
        for q in result:
            return q['numberlookup']
    except Exception,e:
        print '---->ERROR:',e
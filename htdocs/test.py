#!/usr/bin/env python
# encoding: utf-8
import os, sys, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bachviet.settings")
#from django.core.management import execute_from_command_line
from bachviet.apps.tt_parser.models import ExcelParser
from bachviet.apps.tt_dictionary.models import TTSearchEngine
from bachviet.apps.tt_dictionary.models import SearchTest
from django.test import TestCase

#tmp = ExcelParser()
#tmp.parse()

#test = TTSearchEngine()
#text_search = test.search('gehen')
test = SearchTest()
text_search = test.search2('',text='gegangen')
#print text_search
for i in range(len(text_search)):
    print '%d - %s - %d' % (text_search[i]['wId'], text_search[i]['wText'], text_search[i]['wtId'])
    #print text_search[i][u'wtText'].encode('utf8')
print 15*'-'
print len(text_search)
assert 'gegangen' in [text_search[i]['wText'] for i in range(len(text_search))]


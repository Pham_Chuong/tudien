from setuptools import setup, find_packages
import os

CLASSIFIERS = [
    'Environment :: Web Environment',
    'Framework :: Django',
    'Intended Audience :: Developers',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Software Development :: Libraries :: Application Frameworks',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
]

setup(
    author="Pham Chuong",
    author_email="hoangchuongk10@gmail.com",
    name='BACH-VIET',
    version= '1.0',
    description='Django web application',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
    license='BSD License',
    platforms=['OS Independent'],
    classifiers=CLASSIFIERS,
    install_requires=[
        'coverage==3.7.1',
        'Django==1.7',
        'django-extensions==1.4.0',
        'django-jenkins==0.16.3',
        'django-tastypie==0.12.2',
        'jslint==0.6.0',
        'MySQL-python==1.2.5',
        'pbr==0.10.0',
        'pep8==1.5.7',
        'pyflakes==0.8.1',
        'python-dateutil==2.4.2',
        'python-jenkins==0.4.0',
        'python-mimeparse==0.1.4',
        'selenium==2.43.0',
        'six==1.8.0',
        'wheel==0.24.0'

    ],
    packages=find_packages(exclude=["htdocs", "htdocs.*"]),
    include_package_data=True,
    zip_safe=False,
)
